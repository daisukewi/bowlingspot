package com.bowlingspot;


import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;
import com.bowlingspot.game.Juego;
import com.bowlingspot.hud.Mensaje;
import com.bowlingspot.hud.Pantalla;
import com.bowlingspot.hud.SplashScreen;
import com.bowlingspot.hud.menu.MenuPrincipal;
import com.jme.input.KeyInput;


/**
 * Clase principal de nuestro juego. Hace las llamadas correspondientes
 * para gestionar los componentes y que se pinten correctamente en pantalla
 * @author Daniel
 */
public class BowlingSpot extends GameComponent {

    public final int ESTADO_SPLASH_SCREEN = 0;
    public final int ESTADO_MENSAJE_CONECTAR_SPOT = 1;
    public final int ESTADO_MENU_PRINCIPAL = 2;
    public final int ESTADO_CARGANDO_JUEGO = 3;
    public final int ESTADO_JUGANDO = 4;

    private GameComponent componente_activo;
    private Mensaje mensaje;
    private Pantalla cargando;
    private Juego theGame;
    private int num_jugadores;


    public BowlingSpot() {
        super("Game-BowlingSpot");
    }

    @Override
    public void initGame() {
    	// TODO:
        //mostrarPantallaPrincipal();
        cargarJuego();
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);

        int estado = this.getEstate();
        switch (estado) {
            case ESTADO_SPLASH_SCREEN:
                if (inicio_estado >= 3.0f)
                    mostrarEncenderSpot();
                break;
            case ESTADO_MENSAJE_CONECTAR_SPOT:
            	// TODO:
                //if (ComponentMannager.getTspInstance().getconected())
                    try {
                        mostrarMenu();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(BowlingSpot.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (LineUnavailableException ex) {
                        Logger.getLogger(BowlingSpot.class.getName()).log(Level.SEVERE, null, ex);
                    }
                break;
            case ESTADO_MENU_PRINCIPAL:
                if (!componente_activo.isActive())
                    if (componente_activo.getCondicionFin() != NINGUNA)
                        cargarJuego();
                    else
                        finalizarJuego();
                break;
            case ESTADO_CARGANDO_JUEGO:
            	if (inicio_estado >= 3.0f)
	            	try {
	                    theGame.load(num_jugadores);
	                    ComponentMannager.removeComponent(cargando);
	                    iniciarJuego();
	                } catch (IOException ex) {
	                    Logger.getLogger(BowlingSpot.class.getName()).log(Level.SEVERE, null, ex);
	                    finalizarJuego();
	                }
                break;
            case ESTADO_JUGANDO:
            	if (KeyInput.get().isKeyDown(KeyInput.KEY_ESCAPE))
            		finalizarJuego();
            	break;
        }
    }

	private void mostrarPantallaPrincipal() {
        componente_activo = new SplashScreen("splash_screen");
        ComponentMannager.addComponent(componente_activo);
        this.setEstate(ESTADO_SPLASH_SCREEN);
    }

    private void mostrarEncenderSpot() {
        mensaje = new Mensaje("conectar_spot");
        mensaje.traslate(0f, -200f);
        ComponentMannager.addComponent(mensaje);
        this.setEstate(ESTADO_MENSAJE_CONECTAR_SPOT);
    }

    private void mostrarMenu() throws LineUnavailableException, InterruptedException {
        ComponentMannager.removeComponent(mensaje);
        ComponentMannager.removeComponent(componente_activo);        
        componente_activo = new MenuPrincipal("main_screen");
        ComponentMannager.addComponent(componente_activo);
        this.setEstate(ESTADO_MENU_PRINCIPAL);
    }

    private void cargarJuego() {
    	cargando = new Pantalla("Carga", "cargando");
    	theGame = new Juego();
    	// TODO:
    	//num_jugadores = componente_activo.getCondicionFin();
    	//ComponentMannager.removeComponent(componente_activo);
    	num_jugadores = 1;
        ComponentMannager.addComponent(cargando);
		this.setEstate(ESTADO_CARGANDO_JUEGO);
	}
    
    private void iniciarJuego() {
        ComponentMannager.addComponent(theGame);
        theGame.setVisible(true);
        theGame.reiniciarComponente();
        this.setEstate(ESTADO_JUGANDO);
    }

    private void finalizarJuego() {
        ComponentMannager.setFinJuego(true);
    }

 }

