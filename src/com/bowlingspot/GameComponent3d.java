package com.bowlingspot;



import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import com.jme.scene.Spatial;
import com.jme.util.export.binary.BinaryImporter;
import com.jme.util.resource.ResourceLocatorTool;
import com.jmex.model.ModelFormatException;
import com.jmex.model.converters.ObjToJme;
import com.jmex.model.ogrexml.MaterialLoader;
import com.jmex.model.ogrexml.OgreLoader;
import com.jmex.model.util.ModelLoader;
import com.jmex.physics.PhysicsSpace;


/**
 * Clase principal para todos los objetos 3D.
 * @author Daniel
 */
public class GameComponent3d extends GameComponent {
	
	private final String model_path = "res/models/";
	private final String text_path = "res/textures/";
	
	protected PhysicsSpace physicsspace;

	public GameComponent3d(String name) {
		super(name);
		physicsspace = ComponentMannager.getPhysicsSpace();
	}
	
	public Spatial loadModelSP(String modelFile) {
        Spatial model0 = null;
        ByteArrayOutputStream BO = new ByteArrayOutputStream();
        URL modelURL = ModelLoader.class.getClassLoader().getResource(model_path + modelFile);
        URL textureURL = ModelLoader.class.getClassLoader().getResource(text_path);

        ObjToJme converter = new ObjToJme();
        try {
            converter.setProperty("mtllib", modelURL);
            converter.setProperty("texdir", textureURL);
            converter.convert(modelURL.openStream(), BO);
            model0 = (Spatial) BinaryImporter.getInstance().load(
                    new ByteArrayInputStream(BO.toByteArray()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return model0;
    }
	
	protected Spatial loadMeshModel(int jugador) throws URISyntaxException, IOException, ModelFormatException {
		Spatial model = null;
		OgreLoader loader = new OgreLoader();
		MaterialLoader matLoader = new MaterialLoader();
		
		String matUrlString = "/" + text_path + "jugador" + jugador + ".material";
		String meshUrlString = "/" + model_path + "playmobil.mesh.xml";
		
		URL matURL = ResourceLocatorTool.locateResource(
		        ResourceLocatorTool.TYPE_TEXTURE, matUrlString);
		URL meshURL = ResourceLocatorTool.locateResource(
		        ResourceLocatorTool.TYPE_MODEL, meshUrlString);

        matLoader.load(matURL.openStream());
        if (matLoader.getMaterials().size() > 0) {
            loader.setMaterials(matLoader.getMaterials());
        }
		    
	    model = loader.loadModel(meshURL);
	    model.setName("PlayMobil");
		
		return model;
	}
	
	protected Spatial loadMeshModel_T() throws URISyntaxException, IOException, ModelFormatException {
		Spatial model = null;
		OgreLoader loader = new OgreLoader();

		String meshUrlString = "/" + model_path + "playmobil.mesh.xml";
		
		URL meshURL = ResourceLocatorTool.locateResource(
		        ResourceLocatorTool.TYPE_MODEL, meshUrlString);

	    model = loader.loadModel(meshURL);
	    model.setName("PlayMobil");

		return model;
	}

}
