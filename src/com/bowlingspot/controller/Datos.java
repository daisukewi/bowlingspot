package com.bowlingspot.controller;



/**
 * Clase que actua como interfaz entre el SPOT y el resto de la
 * aplicacion. Guarda todos los datos del SPOT para cuando le sean
 * preguntados.
 */
public class Datos {

    private double AccX = 0,  AccY = 0,  AccZ = 0; //Guardaremos en estas variables las aceleraciones de los ejes X, Y y Z respectivamente
    private double IncX = 0,  IncY = 0,  IncZ = 0;//Guardaremos en estas variables las inclinaciones de los ejes X, Y y Z respectivamente
    private double SW1 = 0,  SW2 = 0; // Guardamos en estas variables lso estados de los botones, si estan a uno significa que estan presionados.




   public Datos()
   {
        this.IncX = 0;
        this.IncY = 0;
        this.IncZ = 0;
        this.AccX = 0;
        this.AccY = 0;
        this.AccZ = 0;
        this.SW1 = 0;
        this.SW2 = 0;
   }

    // Zona de GET

    public double getAccX() {
        return this.AccX;
    } //Devuelve el Valor Guardado en AccX

    public double getAccY() {
        return this.AccY;
    }//Devuelve el Valor Guardado en AccY

    public double getAccZ() {
        return this.AccZ;
    }//Devuelve el Valor Guardado en AccZ

    public double getIncX() {
        return this.IncX;
    }//Devuelve el Valor Guardado en IncX

    public double getIncY() {
        return this.IncY;
    }//Devuelve el Valor Guardado en IncY

    public double getIncZ() {
        return this.IncZ;
    }//Devuelve el Valor Guardado en IncZ

    public double getSW1() {
        return this.SW1;
    }//Devuelve el valor Guardado en SW1

    public double getSW2() {
        return this.SW2;
    }//Devuelve el valor Guardado en SW1


//Zona de SET
    public void setAccX(double AcX) {
        this.AccX = Trunc(AcX, 2);
    } // Establece el Valor de AccX

    public void setAccY(double AcY) {
        this.AccY = Trunc(AcY, 2);
    }// Establece el Valor de AccY

    public void setAccZ(double AcZ) {
        this.AccZ = Trunc(AcZ, 2);
    }// Establece el Valor de AccZ

    public void setIncX(double InX) {
        this.IncX = Trunc(InX, 2);
    }// Establece el Valor de IncX

    public void setIncY(double InY) {
        this.IncY = Trunc(InY, 2);
    }//Establece el Valor de IncY

    public void setIncZ(double InZ) {
        this.IncZ = Trunc(InZ, 2);
    }//Establece el Valor de IncZ

    public void setSW1(double SW) {
        this.SW1 = SW;
    }//Establece el valor de SW1

    public void setSW2(double SW) {
        this.SW2 = SW;
    }//Establece el valor de SW2

   public double Trunc(double nD, int nDec) {
        //Trunca los valores al n�mero de decimales selecionado.

        if (nD > 0) {
            nD = Math.floor(nD * Math.pow(10, nDec)) / Math.pow(10, nDec);
        } else {
            nD = Math.ceil(nD * Math.pow(10, nDec)) / Math.pow(10, nDec);
        }
        // System.out.println("Truncado = "+ nD);
        return nD;
    }



 

    void recibecompleto(String recibed) {
        //Recibe, recorta, y guarda los datos del spot.
        int i = 0;
        while (recibed.contains("$")) {
            adouble(recibed.substring(0, recibed.indexOf("$")), i);
            recibed = recibed.substring(recibed.indexOf("$") + 1, recibed.length());
            i++;
        }
    }

    private void adouble(String sub, int i) {
       //Guarda los valores en sus respectivos lugares.
        switch (i) {
            case 0:
                this.setAccX(Double.valueOf(sub));
                break;
            case 1:
                this.setAccY(Double.valueOf(sub));
                break;
            case 2:
                this.setAccZ(Double.valueOf(sub));
                break;
            case 3:
                this.setIncX(Math.toDegrees(Double.valueOf(sub)));
                break;
            case 4:
                this.setIncY(Math.toDegrees(Double.valueOf(sub)));
                break;
            case 5:
                this.setIncZ(Math.toDegrees(Double.valueOf(sub)));
                break;
            case 6:
                this.setSW1(Double.valueOf(sub));
            case 7:
                this.setSW2(Double.valueOf(sub));
        }
    }

    public double AlphaTo360() {
        double angulo = 0;
        //1er Cuadrante
        if ((((this.IncZ > 0))) && ((this.getIncY() < 0))) {
            angulo = -this.IncY;
        } else {// 2� Cuadrante
            if ((this.IncZ < 0) && (this.IncY < 0)) {
                angulo = 180 + this.IncY;
            } else { //3er Cuadrante
                if (((this.IncY > 0) && (this.IncZ < 0))) {
                    angulo = 180 + this.IncY;
                } else {
                    angulo = 270 + (90 - this.IncY);
                }

            }

        }
        return angulo;
    }

    public double Yto360() {
        double y360 = 0;
        if (this.IncY < 0) {
            if (((this.IncZ > 0) || ((this.IncZ < 0) && (this.IncX < 0)))) {
                y360 = -this.IncY;
            }

        }


        return y360;
    }

  public double YtoA()
  {return ((AlphaTo360()+90)%360);}


    public boolean isleft() {
        return (this.getIncX() < -30);
    }

    public boolean isright() {
        return (this.getIncX() > 30);
    }

    public boolean isup() {
        return (this.getIncY() < -30);
    }

    public boolean isdown() {
        return (this.getIncY() > 30);
    }

    public boolean isb1() {
        return (this.getSW1() == 1);
    }

    public boolean isb2() {
        return (this.getSW2() == 1);
    }
}