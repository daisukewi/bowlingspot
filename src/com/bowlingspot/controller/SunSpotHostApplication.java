package com.bowlingspot.controller;

/*
 * SunSpotHostApplication.java
 *
 * Created on 24-feb-2010 11:48:58;
 */


import com.sun.spot.peripheral.radio.RadioFactory;
import com.sun.spot.peripheral.radio.IRadioPolicyManager;
import com.sun.spot.io.j2me.radiostream.*;
import com.sun.spot.io.j2me.radiogram.*;
import com.sun.spot.util.IEEEAddress;
import com.sun.spot.util.Utils;
import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.microedition.io.*;

/**
 * Sample Sun SPOT host application
 */
public class SunSpotHostApplication {

    private RadiogramConnection conn;
    private DatagramConnection conn2;
    private Datagram dtr,  dts;
    private Radiogram dg;
    private String ultimo = "";
    private double nmensajes=0;
  

    public void run() throws IOException {
        conn = (RadiogramConnection) Connector.open("radiogram://:100");
        dtr = conn.newDatagram(conn.getMaximumLength(), "0014.4F01.0000.5BFE");
        dts = conn.newDatagram(conn.getMaximumLength(), "0014.4F01.0000.5BFE");
        dg = (Radiogram) dtr;
        conexion();
        
    }


   private void conexion() throws IOException {
        recibir();
        if (!(dg.readUTF().equals("M1"))) {
            System.out.println("Error Recepcion M1");
        }
        dts.reset();
        dts.writeUTF("M2");
        conn.send(dts);
        reciboconfirmacion();

    }



    public Datos DameTiltAcc() throws IOException {
        
        Datos datos = new Datos();
        MandaMenu("TiltAcc");
        datos =this.recibestring();
        return datos;
    }

     public void recibir() throws IOException {
        dtr.reset();
        dg.reset();
        conn.receive(dtr);
        dg = (Radiogram) dtr;
        dg.resetRead();
        dg.resetRead();
        control();
    }
    private Datos recibestring() throws IOException{
     conn.receive(dtr);
     dg = (Radiogram) dtr;
     dg.resetRead();
     control();
     Datos datos = new Datos();
     //System.out.println("Recibido Largo= "+dg.readUTF());
     dg.resetRead();
     datos.recibecompleto(dg.readUTF());
     return datos;
    }

    private void reciboconfirmacion() throws IOException {
        recibir();
        if (!(dg.readUTF().equals("OK"))) {
            //System.out.println("KO");
            Utils.sleep(50000);
        } 

        dg.resetRead();
    }

    private void MandaMenu(String STR) throws IOException {
        enviar(STR + (nmensajes++));
        if (nmensajes==((Double.MAX_VALUE)-10))
         {nmensajes=0;}
    }

    public void enviar(String str) throws IOException {
        dts.reset();
        dts.writeUTF(str);
        conn.send(dts);
        dts.reset();
    }

   
    public void control() throws IOException {
        if (dg.readUTF().equals(ultimo)) {
            //System.out.println("Repetido");
            recibir();
        } else {
            dg.resetRead();
            ultimo = dg.readUTF();
            dg.resetRead();
        }

    }
    public int inizdch() throws IOException {
        int estado = 1;
        Datos datos = new Datos();
        datos = DameTiltAcc();
        //System.out.println((datos.getIncX()));
        if ((datos.getIncX()) <= -30) {
            estado = 2;
        } else if ((datos.getIncX()) >= 30) {
            estado = 3;
        }
        return estado;
    }
    public int posicionhorizontal(Datos dt) throws IOException
    {
       int aux=0;
       aux=(int) Math.round(dt.getIncX());
       if ((aux != -1)&&(aux != 1)&&(aux != 0))
        {if (aux>0) aux-=1;
         else aux+=1;}
       else {aux=0;}
     return aux;
    }
}
