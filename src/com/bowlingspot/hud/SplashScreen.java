package com.bowlingspot.hud;



/**
 * SplashScreen
 * <p>
 * Clase que se encarga de manejar la pantalla de inicio que se muestra
 * mientras se cargan los datos para jugar.
 */
public class SplashScreen extends Pantalla {

    public SplashScreen (String background_img) {
        super("SplashScreen", background_img);
    }

    @Override
    protected void onEnabled(float time_enabling) {
        super.onEnabled(time_enabling);

        this.setScale(time_enabling / this.fin_animacion, time_enabling / this.fin_animacion);
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
    }

}


