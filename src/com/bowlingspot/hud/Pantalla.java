package com.bowlingspot.hud;



/**
 * Pantalla
 * <p>
 * De ella hereda cualquier gr�fico que se tenga que pintar a pantalla completa.
 */
public class Pantalla extends HudComponent {

    public Pantalla (String name, String background_img) {
        super ("Pantalla-"+name);
        loadImage(display.getWidth(), display.getHeight(), background_img);
        mapImage();
        centerScreen();
    }

    private void centerScreen () {
        this.setPosition(display.getWidth() / 2, display.getHeight() / 2);
    }

	@Override
	public void update(float tpf) {
		super.update(tpf);
	}

}
