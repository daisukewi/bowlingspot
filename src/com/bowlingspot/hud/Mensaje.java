package com.bowlingspot.hud;



/**
 * HudComponent
 * <p>
 * Representa cualquier objeto que tenga que pintarse en una porci�n de pantalla
 * @author Daniel
 */
public class Mensaje extends HudComponent {

    public Mensaje (String msg_image) {
        super ("Mensaje-"+msg_image);

        loadImage(msg_image);
        mapImage();
        setTransparent();
        setPosition(display.getWidth() / 2, display.getHeight() / 2);
        
        //Aqu� es donde se indica qu� tama�o relativo tiene el mensaje
        this.setRelativeHeightScale(0.1f);
    }

}
