package com.bowlingspot.hud;


import java.net.URL;
import java.nio.FloatBuffer;
import com.bowlingspot.GameComponent;
import com.jme.image.Texture;
import com.jme.math.Vector2f;
import com.jme.math.Vector3f;
import com.jme.renderer.Renderer;
import com.jme.scene.TexCoords;
import com.jme.scene.shape.Quad;
import com.jme.scene.state.BlendState;
import com.jme.scene.state.LightState;
import com.jme.scene.state.TextureState;
import com.jme.util.TextureManager;
import com.jme.util.geom.BufferUtils;
import com.jme.util.resource.ResourceLocatorTool;

/**
 * HudComponent
 * <p>
 * Esta clase se encarga de pintar las cosas en la pantalla y gestionar toda
 * la lógica de los gráficos.
 * @author Daniel
 */
public abstract class HudComponent extends GameComponent {

	private static final String path = "/res/img/";

    private Quad hud_image;
    protected float texture_width;
    protected float texture_height;
    protected int width;
    protected int height;
    private Vector2f actual_scale;
    private BlendState bs;
    private TextureState ts;

    public HudComponent (String name) {
        super (name);

        LightState ls = display.getRenderer().createLightState();
        ls.setEnabled(false);
        root_node.setRenderState(ls);
        root_node.updateRenderState();
        actual_scale = new Vector2f(1.0f, 1.0f);
    }

    protected void mapImage () {
        mapImage(new Vector2f(0,0), new Vector2f(texture_width, texture_height));
    }

    protected void mapImage(Vector2f inicio, Vector2f fin) {
        FloatBuffer texCoords = BufferUtils.createVector2Buffer(4);
            texCoords.put(getUForPixel((int) inicio.x)).put(getVForPixel((int) inicio.y));
            texCoords.put(getUForPixel((int) inicio.x)).put(getVForPixel((int) fin.y));
            texCoords.put(getUForPixel((int) fin.x)).put(getVForPixel((int) fin.y));
            texCoords.put(getUForPixel((int) fin.x)).put(getVForPixel((int) inicio.y));
        hud_image.setTextureCoords(new TexCoords(texCoords));
    }

    protected void loadTexture(String img_name) {
        String img_path = path + img_name + ".png";

        ts = display.getRenderer().createTextureState();
        URL imgURL = ResourceLocatorTool.locateResource(
		        ResourceLocatorTool.TYPE_TEXTURE, img_path);
        
        Texture texture = TextureManager.loadTexture(
                imgURL,
                Texture.MinificationFilter.Trilinear,
                Texture.MagnificationFilter.Bilinear,
                0.0f,
                true);
        ts.setTexture(texture);
        texture_width = ts.getTexture().getImage().getWidth();
        texture_height = ts.getTexture().getImage().getHeight();
        ts.setEnabled(true);
        root_node.setRenderState(ts);
    }

    protected void loadImage(String img_name) {
        loadTexture(img_name);
        this.height = (int) texture_height;
        this.width = (int) texture_width;
        createHud(img_name);
    }

    protected void loadImage(int width, int height, String img_name) {
        loadTexture(img_name);
        this.height = height;
        this.width = width;
        createHud(img_name);
    }

    private float getUForPixel (int xPixel) {
        return (float) xPixel / texture_width;
    }

    private float getVForPixel (int yPixel) {
        return 1f - (float) yPixel / texture_height;
    }

    protected void createHud(String img_name) {
        root_node.detachChild(hud_image);
        hud_image = new Quad("HUD-" + img_name, width, height);
        hud_image.setRenderQueueMode(Renderer.QUEUE_ORTHO);
        root_node.attachChild(hud_image);
    }
    
    public void setTransparent () {
        bs = display.getRenderer().createBlendState();
        bs.setBlendEnabled(true);
        bs.setSourceFunctionAlpha(BlendState.SourceFunction.SourceAlpha);
        bs.setDestinationFunctionAlpha(BlendState.DestinationFunction.OneMinusSourceAlpha);
        //bs.setBlendEquation(BlendState.BlendEquation.Subtract);
        bs.setTestEnabled(false);
        bs.setEnabled(true);
        root_node.setRenderState(bs);
    }

    public void setPosition (float x, float y) {
        root_node.getLocalTranslation().set(new Vector3f(x, y, 0));
    }
    
    public void setPositionCenter (float x, float y) {
    	setPosition(x - width / 2, y - height / 2);
    }

    public Vector2f getPosition () {
        return new Vector2f(root_node.getLocalTranslation().x, root_node.getLocalTranslation().y);
    }

    public void setScale (float x, float y) {
        root_node.getLocalScale().set(new Vector3f(x * actual_scale.x,
                y * actual_scale.y,
                1.0f));
    }

    public void traslate (float x, float y) {
        this.setPosition(this.getPosition().x + x, this.getPosition().y + y);
    }

    public void setRelativeWidthScale (float scale) {
        float ratio = (float)this.height / (float)this.width;
        float ancho_buscado = display.getWidth() * scale;
        float alto_buscado = display.getHeight() * scale * ratio;

        actual_scale.x = ancho_buscado / this.width;
        actual_scale.y = alto_buscado / this.height;
        setScale(1.0f, 1.0f);
    }

    public void setRelativeHeightScale (float scale) {
        float ratio = (float)this.height / (float)this.width;
        float ancho_buscado = display.getWidth() * scale / ratio;
        float alto_buscado = display.getHeight() * scale;

        actual_scale.x = ancho_buscado / this.width;
        actual_scale.y = alto_buscado / this.height;
        setScale(1.0f, 1.0f);
    }

    public float getWidth() {
        return this.width * this.actual_scale.x;
    }

    public float getHeight() {
        return this.height * this.actual_scale.y;
    }
}
