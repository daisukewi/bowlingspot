package com.bowlingspot.hud.menu;



import com.bowlingspot.hud.HudComponent;



/**
 * 
 */
public abstract class SubMenu extends HudComponent {

    protected final int espaciado_horizontal = 10;

    public Boton boton_izq;
    public Boton boton_der;
    public Progreso barra_progreso;
    
    private float posicion_y;
    private float separacion;

    public SubMenu (float posicion_y, float separacion) {
        super("SubMenu");

        this.posicion_y = posicion_y;
        this.separacion = separacion / 2 + espaciado_horizontal;
        this.setPosition(display.getWidth() / 2, display.getHeight() / 2);
        this.duracionAnimacion(0.7f);
    }

    /**
     * Añade el botón izquierdo al submenú.
     * @param imagen nombre de la imagen a utilizar para el botón.
     */
    protected void addBotonIzquierdo(String imagen) {
        boton_izq = new Boton(imagen);

        //boton.setRelativeHeightScale(0.1f);
        separacion += boton_izq.getWidth() / 2;
        boton_izq.setPosition((display.getWidth() / 4) - separacion, posicion_y);
        root_node.attachChild(boton_izq.getRootNode());
    }

    /**
     * Añade el botón derecho al submenú.
     * @param imagen nombre de la imagen a utilizar para el botón.
     */
    protected void addBotonDerecho(String imagen) {
        boton_der = new Boton(imagen);

        //boton.setRelativeHeightScale(0.1f);
        boton_der.setPosition(separacion, posicion_y);
        root_node.attachChild(boton_der.getRootNode());
    }

    /**
     * Añade una barra de progreso sobre el botón que invocó al submenu.
     */
    protected void addBarraProgreso() {
        barra_progreso = new Progreso();

        barra_progreso.setPosition(0, posicion_y);
        root_node.attachChild(barra_progreso.getRootNode());
    }

    /**
     * Selecciona el botón de la izquierda
     */
    public void seleccionarIzquierdo() {
        boton_izq.setSeleccionado(true);
        boton_der.setSeleccionado(false);
    }

    /**
     * Selecciona el botón de la derecha
     */
    public void seleccionarDerecho() {
        boton_izq.setSeleccionado(false);
        boton_der.setSeleccionado(true);
    }

    /**
     * Desselecciona ambos botones
     */
    public void deseleccionar() {
        boton_izq.setSeleccionado(false);
        boton_der.setSeleccionado(false);
    }

    public boolean isDerechoSeleccionado() {
        return boton_der.getSeleccionado();
    }

    public boolean isIzquierdoSeleccionado() {
        return boton_izq.getSeleccionado();
    }

    @Override
    protected void onEnabled(float timeEnabling) {
        super.onEnabled(timeEnabling);

        float restante = timeEnabling / this.fin_animacion;
        float recorrido = this.separacion * restante;
        float inicio = 0;
        boton_izq.setPosition(inicio - recorrido, this.posicion_y);
        boton_der.setPosition(inicio + recorrido, this.posicion_y);
        boton_izq.setScale(restante, restante);
        boton_der.setScale(restante, restante);
    }

    @Override
    protected void onDisabled(float timeDisabling) {
        super.onDisabled(timeDisabling);

        float restante = timeDisabling / this.fin_animacion;
        float recorrido = this.separacion - (this.separacion * restante);
        float inicio = 0;
        boton_izq.setPosition(inicio - recorrido, this.posicion_y);
        boton_der.setPosition(inicio + recorrido, this.posicion_y);
        boton_izq.setScale( 1 - restante, 1 - restante);
        boton_der.setScale( 1 - restante, 1 - restante);
    }

}
