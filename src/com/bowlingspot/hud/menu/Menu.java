package com.bowlingspot.hud.menu;



import java.util.ArrayList;
import java.util.Iterator;
import com.bowlingspot.ComponentMannager;
import com.bowlingspot.hud.Pantalla;

/**
 * 
 */
public abstract class Menu extends Pantalla {
    
    private final float espaciado_vertical = 20.0f;

    private int actual_height;

    public ArrayList<Boton> botones;

    public Menu (String background) {
        super("Menu", background);
        botones = new ArrayList<Boton>();
        actual_height = 0;
        this.duracionAnimacion(0.7f);
    }

    /**
     * Añade un nuevo botón al menú.
     * @param imagen nombre de la imagen a utilizar para el botón.
     */
    protected void addBoton(String imagen) {
        Boton boton = new Boton(imagen);
        botones.add(boton);
        //boton.setRelativeHeightScale(0.1f);
        actual_height += (actual_height == 0) ? boton.getHeight() : espaciado_vertical + boton.getHeight();
        reposicionarBotones();
        root_node.attachChild(boton.getRootNode());
    }

    /**
     * Realiza la acción correspondiente a seleccionar el botón superior.
     */
    public void seleccionarSuperior() {
        int i;
        for (i = 1; i < botones.size(); i++)
            if (botones.get(i).getSeleccionado()) {
                seleccionar(i - 1);
                ComponentMannager.getGMInstance().play("clip.wav" , 6);
            }
        this.setEstate(this.getEstate());
    }

    /**
     * Realiza la acción correspondiente a seleccionar el botón inferior.
     */
    public void seleccionarInferior() {
        int i;
        for (i = botones.size() - 2; i >= 0; i--)
            if (botones.get(i).getSeleccionado()) {
                seleccionar(i + 1);
                ComponentMannager.getGMInstance().play("clip.wav" , 6);
            }
        this.setEstate(this.getEstate());
    }

    /**
     * Realiza la acción correspondiente al pulsar el botón de Atras.
     */
    public void seleccionarCancelar() {
        ComponentMannager.getGMInstance().play("undo.wav",6);
        this.terminar();
    }

    /**
     * Devuelve el botón que está seleccionado
     * @return un entero correspondiente al numero de boton seleccionado
     */
    public int getSeleccionado() {
        int i;
        for (i = 0; i < botones.size(); i++) {
            if (botones.get(i).getSeleccionado())
                return i;
        }
        return 0;
    }

    /**
     * Recalcula las posiciones de los botones par que estén centrados en
     * pantalla
     */
    private void reposicionarBotones() {
        int loc_x = 0;
        int loc_y = actual_height / 2;

        Iterator<Boton> i = botones.iterator();
        while (i.hasNext()) {
            Boton bot = (Boton)i.next();
            bot.setPosition(loc_x, loc_y);
            loc_y -= espaciado_vertical + bot.getHeight();
        }
    }

    /**
     * Selecciona un boton
     */
    protected void seleccionar(int boton) {
        Iterator<Boton> i = botones.iterator();
        while (i.hasNext()) {
            ((Boton)i.next()).setSeleccionado(false);
        }
        botones.get(boton).setSeleccionado(true);
    }


    @Override
    protected void onEnabled(float timeEnabling) {
        super.onEnabled(timeEnabling);

        float recorrido = 3 * (float)display.getWidth() / 2;
        float restante = timeEnabling / (float)this.fin_animacion;
        float total_x = recorrido - display.getWidth() * restante;
        total_x = (total_x < display.getWidth() / 2)? display.getWidth() / 2 : total_x;
        this.setPosition(total_x, display.getHeight() / 2);
    }

}