package com.bowlingspot.hud.menu;



import com.bowlingspot.hud.HudComponent;
import com.jme.math.Vector2f;

/**
 * Boton
 * <p>
 * Representa a un bot�n en 2D que se pinta encima de un men�.
 */
public class Boton extends HudComponent {

    private boolean seleccionado;


    /**
     * Boton
     * @param image textura con la imagen del boton seleccionado y no seleccionado
     */
    public Boton (String image) {
        super ("Boton-"+image);

        this.loadTexture(image);
        this.width = (int)(this.texture_width / 2);
        this.height = (int)this.texture_height;
        this.createHud(image);
        setSeleccionado(false);
        setTransparent();

        //Aquí es donde se indica qué tamaño relativo tiene el boton
        //this.setRelativeHeightScale(0.07f);
    }

    /**
     * Marca el boton como seleccionado o no
     * @param estado true = lo selecciona. false = lo desselecciona.
     */
    public void setSeleccionado (boolean estado) {
        this.seleccionado = estado;

        if (estado)
            this.mapImage(new Vector2f(0,0), new Vector2f(this.width, this.height));
        else
            this.mapImage(new Vector2f(this.width+1, 0), new Vector2f(this.width*2, this.height));
    }
    
    public boolean getSeleccionado () {
        return this.seleccionado;
    }

}
