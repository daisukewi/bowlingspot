package com.bowlingspot.hud.menu;



import javax.sound.sampled.*;


/**
 * 
 */
public class SubmenuVolumen extends SubMenu {

    private float volumen_inicial;
    private float volumen_actual;
    private Port lineOut;
    FloatControl controlIn;
    private float timein=0;

    public SubmenuVolumen(float posicion_y, int separacion) throws LineUnavailableException {
        super(posicion_y, separacion);

        this.addBotonIzquierdo("boton_menos");
        this.addBotonDerecho("boton_mas");
        this.addBarraProgreso();
        this.IniSoundSystem();
        this.reiniciarComponente();
    }

    private void IniSoundSystem() throws LineUnavailableException {

        if (AudioSystem.isLineSupported(Port.Info.LINE_OUT)) {
            lineOut = (Port) AudioSystem.getLine(Port.Info.LINE_OUT);
            lineOut.open();
        } else if (AudioSystem.isLineSupported(Port.Info.HEADPHONE)) {
            lineOut = (Port) AudioSystem.getLine(Port.Info.HEADPHONE);
            lineOut.open();
        } else if (AudioSystem.isLineSupported(Port.Info.SPEAKER)) {
            lineOut = (Port) AudioSystem.getLine(Port.Info.SPEAKER);
            lineOut.open();
        } else {
            System.out.println("Unable to get Output Port");
            return;
        }
        controlIn = (FloatControl) lineOut.getControl(FloatControl.Type.VOLUME);
        volumen_inicial = controlIn.getValue();
    }

    private void setVolumen(float vol) {
        controlIn.setValue(vol);
    }

    private void setSystemVolume(String vol) {

        if (vol.equals("More")) {
            if (volumen_actual < .25) {
                controlIn.setValue((float) 0.25);
                volumen_actual = (float) .25;
                
            } else if (volumen_actual < .50) {
                controlIn.setValue((float) .5);
                volumen_actual = (float) .50;
                
            } else if (volumen_actual < .75) {
                controlIn.setValue((float) .75);
                volumen_actual = (float) .75;
                
            } else if (volumen_actual < 1) {
                controlIn.setValue((float)1);
                volumen_actual = 1;
            }
        } else {
            if (volumen_actual > .75) {
                controlIn.setValue((float) .75);
                volumen_actual = (float) .75;
            } else if (volumen_actual > .50) {
                controlIn.setValue((float) .5);
                volumen_actual = (float) .50;
            } else if (volumen_actual > .25) {
                controlIn.setValue((float) .25);
                volumen_actual = (float) .25;
            } else if (volumen_actual > 0) {
                controlIn.setValue((float) .0);
                volumen_actual = 0;
            }
        }
    }

    public void aceptarVolumen() {
        //this.FinSoundSystem();
        terminar();
    }

    public void cancelarVolumen() {
        setVolumen(volumen_inicial);
        //this.FinSoundSystem();
        terminar();
    }

    public void bajarVolumen() {
        setSystemVolume("Less");
        barra_progreso.setPorcentaje(volumen_actual);
    }

    public void subirVolumen() {
        setSystemVolume("More");
        barra_progreso.setPorcentaje(volumen_actual);
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
        timein+=(0.06);
        if (spot.isleft()) {
            seleccionarIzquierdo();
        } else if (spot.isright()) {
            seleccionarDerecho();
        } else {
            deseleccionar();
        }
        if (spot.isb2() && timein >= 0.5 && inicio_estado >= 0.5) {
            timein=0;
            cancelarVolumen();
        }
        if (spot.isb1() && timein >= 0.5 && inicio_estado >= 0.5) {
            if (this.isDerechoSeleccionado()) {
                subirVolumen();
                timein=0;
            } else if (isIzquierdoSeleccionado()) {
                bajarVolumen();
                timein=0;
            } else {
                timein=0;
                this.aceptarVolumen();                
            }
        }
    }

    @Override
    public void reiniciarComponente() {
        super.reiniciarComponente();
        volumen_inicial= trunc(controlIn.getValue(),2);
        volumen_actual = volumen_inicial;
        barra_progreso.setPorcentaje(volumen_inicial);
        timein=0;
    }


    /**
     * Trunca los valores al número de decimales selecionado.
     * @param nD
     * @param nDec
     * @return
     */
    private float trunc(float nD, int nDec) {
	    if (nD > 0) {
	        nD = (float) (Math.floor(nD * Math.pow(10, nDec)) / Math.pow(10, nDec));
	    } else {
	        nD = (float) (Math.ceil(nD * Math.pow(10, nDec)) / Math.pow(10, nDec));
	    }
	    return nD;
    }
}
