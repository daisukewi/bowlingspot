package com.bowlingspot.hud.menu;



import javax.sound.sampled.LineUnavailableException;

import com.bowlingspot.ComponentMannager;
import com.bowlingspot.hud.Mensaje;


/**
 * 
 */
public class MenuConfigurar extends Menu {

    /**
     * Constantes de Estados
     */
    public final int ESTADO_MENU_CONFIGURAR = 0;
    public final int ESTADO_SUBMENU_VOLUMEN = 1;
    public final int ESTADO_SUBMENU_BRILLO = 2;
    public final int ESTADO_ANTIALLIASING = 3;
    /**
     * Constantes de nombres de los botones
     */
    private final int BOTON_VOLUMEN = 0;
    private final int BOTON_BRILLO = 1;
    private final int BOTON_ANTIALLIASING = 2;
    private final int BOTON_ATRAS = 3;
    public SubmenuBrillo theSubmenuBrillo;
    public SubmenuVolumen theSubmenuVolumen;
    public Mensaje theAntialliasing;

    public MenuConfigurar(String background) throws LineUnavailableException, InterruptedException {
        super(background);

        this.addBoton("boton_volumen");
        this.addBoton("boton_brillo");
        this.addBoton("boton_antialliasing");
        this.addBoton("boton_atras");
        this.seleccionar(BOTON_VOLUMEN);
        theSubmenuVolumen = new SubmenuVolumen(this.botones.get(BOTON_VOLUMEN).getPosition().y, (int) this.botones.get(BOTON_VOLUMEN).getWidth());
        theSubmenuBrillo = new SubmenuBrillo(this.botones.get(BOTON_BRILLO).getPosition().y, (int) this.botones.get(BOTON_BRILLO).getWidth());
        theAntialliasing = new Mensaje("mensaje_antialliasing");
    }

    private void seleccionarVolumen() {
        GM.play("click.wav",6);
        ComponentMannager.addComponent(theSubmenuVolumen);
        theSubmenuVolumen.reiniciarComponente();
        setEstate(ESTADO_SUBMENU_VOLUMEN);
    }

    private void seleccionarBrillo() {
        GM.play("click.wav",6);
        ComponentMannager.addComponent(theSubmenuBrillo);
        theSubmenuBrillo.reiniciarComponente();
        setEstate(ESTADO_SUBMENU_BRILLO);
    }

    private void seleccionarAntialliasing() {
        GM.play("click.wav",6);
        ComponentMannager.addComponent(theAntialliasing);
        theAntialliasing.reiniciarComponente();
        setEstate(ESTADO_ANTIALLIASING);
    }

    private void seleccionarAtras() {
        GM.play("click.wav",6);
        terminar();
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);

        switch (getEstate()) {
            case ESTADO_MENU_CONFIGURAR:
                if (spot.isdown()&& inicio_estado >= 0.5) {
                    seleccionarInferior();
                }
                if (spot.isup()&& inicio_estado >= 0.5) {
                    seleccionarSuperior();
                }
                if (spot.isb2()&& inicio_estado >= 0.5) {
                    seleccionarCancelar();
                }
                if (spot.isb1()&& inicio_estado >= 0.5) {
                    switch (getSeleccionado()) {
                        case BOTON_VOLUMEN:
                            seleccionarVolumen();
                            break;
                        case BOTON_BRILLO:
                            seleccionarBrillo();
                            break;
                        case BOTON_ANTIALLIASING:
                            seleccionarAntialliasing();
                            break;
                        case BOTON_ATRAS:
                            seleccionarAtras();
                            break;
                    }
                }
                break;
            case ESTADO_SUBMENU_VOLUMEN:
                if (!theSubmenuVolumen.isActive()) {
                    ComponentMannager.removeComponent(theSubmenuVolumen);
                    setEstate(ESTADO_MENU_CONFIGURAR);
                }
                break;
            case ESTADO_SUBMENU_BRILLO:
                if (!theSubmenuBrillo.isActive()) {
                    ComponentMannager.removeComponent(theSubmenuBrillo);
                    setEstate(ESTADO_MENU_CONFIGURAR);
                }
                break;
            case ESTADO_ANTIALLIASING:
                if (spot.isb1() || spot.isb2()) {
                    ComponentMannager.removeComponent(theAntialliasing);
                    setEstate(ESTADO_MENU_CONFIGURAR);
                }
                break;
        }
    }

    @Override
    protected void onDisabled(float timeDisabling) {
        super.onDisabled(timeDisabling);

        float restante = timeDisabling / (float) this.fin_animacion;
        float total_x = (display.getWidth() / 2) + (display.getWidth() * restante);
        setPosition(total_x, display.getHeight() / 2);
    }

}
