package com.bowlingspot.hud.menu;



import com.bowlingspot.hud.HudComponent;
import com.jme.math.Vector2f;

/**
 * 
 */
public class Progreso extends HudComponent {

    private static String nombre_imagen = "progreso";

    /**
     * Barra de progreso
     */
    public Progreso () {
        super ("BarraProgreso-"+nombre_imagen);

        this.loadTexture(nombre_imagen);
        this.width = (int)this.texture_width;
        this.height = (int)(this.texture_height / 5);
        this.createHud(nombre_imagen);
        setTransparent();
        setPorcentaje(0.0f);

        //Aquí es donde se indica qué tamaño relativo tiene el boton
        //this.setRelativeHeightScale(0.07f);
    }

    /**
     * Marca el boton como seleccionado o no
     * @param estado true = lo selecciona. false = lo desselecciona.
     */
    public void setPorcentaje (float porcentaje) {

        if (porcentaje <= 0.0f)
            this.mapImage(new Vector2f(0,0), new Vector2f(this.width, this.height));
        else if (porcentaje <= 0.25f)
            this.mapImage(new Vector2f(0, this.height + 1), new Vector2f(this.width, this.height * 2));
        else if (porcentaje <= 0.50f)
            this.mapImage(new Vector2f(0, this.height * 2 + 1), new Vector2f(this.width, this.height * 3));
        else if (porcentaje <= 0.75f)
            this.mapImage(new Vector2f(0, this.height * 3 + 1), new Vector2f(this.width, this.height * 4));
        else
            this.mapImage(new Vector2f(0, this.height * 4 + 1), new Vector2f(this.width, this.height * 5));
    }

}