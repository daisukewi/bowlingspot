package com.bowlingspot.hud.menu;



import javax.sound.sampled.LineUnavailableException;

import com.bowlingspot.ComponentMannager;
import com.bowlingspot.hud.Creditos;


/**
 * 
 */
public class MenuPrincipal extends Menu {

    /**
     * Constantes de Estados
     */
    public final int ESTADO_MENU_PRINCIPAL = 0;
    public final int ESTADO_SUBMENU_JUGAR = 1;
    public final int ESTADO_MENU_CONFIGURAR = 2;
    public final int ESTADO_CREDITOS = 3;
    public final int ESTADO_SCENARIO = 4;

    /**
     * Constantes de nombres de los botones
     */
    private final int BOTON_JUGAR = 0;
    private final int BOTON_CONFIGURAR = 1;
    private final int BOTON_CREDITOS = 2;
    private final int BOTON_SALIR = 3;

    private SubmenuJugar theSubmenuJugar;
    private MenuConfigurar theMenuConfigurar;
    private Creditos theCreditos;


    public MenuPrincipal (String background) throws LineUnavailableException, InterruptedException {
        super(background);
        this.addBoton("boton_jugar");
        this.addBoton("boton_configurar");
        this.addBoton("boton_creditos");
        this.addBoton("boton_salir");
        this.seleccionar(BOTON_JUGAR);

        theSubmenuJugar = new SubmenuJugar(this.botones.get(BOTON_JUGAR).getPosition().y
                , (int)botones.get(BOTON_JUGAR).getWidth());
        theMenuConfigurar = new MenuConfigurar("main_screen");
        theCreditos = new Creditos("creditos");
    }

    private void seleccionarJugar() {
        ComponentMannager.getGMInstance().play("click.wav",6);
        ComponentMannager.addComponent(theSubmenuJugar);

        theSubmenuJugar.reiniciarComponente();
        setEstate(ESTADO_SUBMENU_JUGAR);
    }

    private void seleccionarConfigurar() {
        ComponentMannager.getGMInstance().play("click.wav",6);
        ComponentMannager.addComponent(theMenuConfigurar);
        theMenuConfigurar.reiniciarComponente();
        setEstate(ESTADO_MENU_CONFIGURAR);
    }

    private void mostrarCreditos() {
        ComponentMannager.getGMInstance().play("click.wav",6);
        ComponentMannager.addComponent(theCreditos);
        theCreditos.reiniciarComponente();
        setEstate(ESTADO_CREDITOS);
    }

    private void salir() {
        ComponentMannager.getGMInstance().play("click.wav",6);
        terminar(SALIR_JUEGO);
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);

        switch (getEstate()) {
            case ESTADO_MENU_PRINCIPAL:
                if (spot.isdown() && inicio_estado >= 0.5)
                    seleccionarInferior();
                if (spot.isup() && inicio_estado >= 0.5)
                    seleccionarSuperior();
                if (spot.isb2() && inicio_estado >= 0.5)
                    seleccionarCancelar();
                if (spot.isb1() && inicio_estado >= 0.5) {
                    switch(getSeleccionado()) {
                        case BOTON_JUGAR:
                            seleccionarJugar();
                            break;
                        case BOTON_CONFIGURAR:
                            seleccionarConfigurar();
                            break;
                        case BOTON_CREDITOS:
                            mostrarCreditos();
                            break;
                        case BOTON_SALIR:
                            salir();
                            break;
                    }
                }
                break;
            case ESTADO_SUBMENU_JUGAR:
                if (!theSubmenuJugar.isActive()) {
                    ComponentMannager.removeComponent(theSubmenuJugar);
                    if (theSubmenuJugar.getCondicionFin() != NINGUNA) {
                        root_node.attachChild(theSubmenuJugar.getRootNode());
                        terminar();
                        setCondicionFin(theSubmenuJugar.getCondicionFin());
                        setEstate(ESTADO_MENU_PRINCIPAL);
                    } else {
                        setEstate(ESTADO_MENU_PRINCIPAL);
                    }
                }
                break;
            case ESTADO_MENU_CONFIGURAR:
                if (!theMenuConfigurar.isActive()) {
                    ComponentMannager.removeComponent(theMenuConfigurar);
                    setEstate(ESTADO_MENU_PRINCIPAL);
                }
                break;
            case ESTADO_CREDITOS:
                if (inicio_estado >= 30) {
                    ComponentMannager.removeComponent(theCreditos);
                    salir();
                }
                 break;
        }
        
    }

    @Override
    protected void onDisabled(float timeDisabling) {
        super.onDisabled(timeDisabling);

        float restante = timeDisabling / fin_animacion;
        setScale(1 - restante, 1 - restante);
    }
}
