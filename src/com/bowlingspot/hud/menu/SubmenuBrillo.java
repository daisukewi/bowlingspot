package com.bowlingspot.hud.menu;



/**
 *
 */
public class SubmenuBrillo extends SubMenu {


    private float brillo_inicial;
    private float brillo_actual;
    private float timein = 0;

    public SubmenuBrillo(float posicion_y, int separacion) {
        super(posicion_y, separacion);

        this.addBotonIzquierdo("boton_menos");
        this.addBotonDerecho("boton_mas");
        this.addBarraProgreso();

        this.reiniciarComponente();
    }

    public void aceptarBrillo() {
        terminar();
    }

    public void aumentarBrillo() {
        if (brillo_actual < -0.25) {
            display.setBrightness((float) -0.25);
            barra_progreso.setPorcentaje((float) 0.25);
        } else if (brillo_actual < 0) {
            display.setBrightness(0);
            barra_progreso.setPorcentaje((float) 0.5);
        } else if (brillo_actual < 0.25) {
            display.setBrightness((float) 0.25);
            barra_progreso.setPorcentaje((float) 0.75);
        } else {
            display.setBrightness((float) 0.5);
            barra_progreso.setPorcentaje((float) 1);
        }
        brillo_actual=display.getBrightness();
    }

    public void disminuirBrillo() {
        if (brillo_actual > 0.25) {
            display.setBrightness((float) .25);
            barra_progreso.setPorcentaje((float) .75);
        } else if (brillo_actual > 0) {
            display.setBrightness((float) 0);
            barra_progreso.setPorcentaje((float) 0.5);
        } else if (brillo_actual > -0.25) {
            display.setBrightness((float) -0.25);
            barra_progreso.setPorcentaje((float) 0.25);
        } else {
            display.setBrightness((float) -0.5);
            barra_progreso.setPorcentaje((float) 0);
        }
        brillo_actual=display.getBrightness();
    }

    public void cancelarBrillo() {
        display.setBrightness((brillo_inicial));
        terminar();
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
        timein += (0.06);

        if (spot.isleft()) {
            seleccionarIzquierdo();
        } else if (spot.isright()) {
            seleccionarDerecho();
        } else {
            deseleccionar();
        }

        if (spot.isb2() && timein >= 0.5 && inicio_estado >= 0.5) {
            timein = 0;
            cancelarBrillo();
        }
        if (spot.isb1() && timein >= 0.5 && inicio_estado >= 0.5) {
            if (isDerechoSeleccionado()) {
                timein = 0;
                aumentarBrillo();
            } else if (isIzquierdoSeleccionado()) {
                timein = 0;
                disminuirBrillo();
            } else {
                timein = 0;
                aceptarBrillo();
            }
        }
    }

    public float pasaporcentaje(float br) {
        if (br <= -.5) {
            return (float) 0;
        } else if (br <= -.25) {
            return (float) .25;
        } else if (br <= 0) {
            return (float) .5;
        } else if (br <= .25) {
            return (float) .75;
        } else {
            return 1;
        }
    }

    @Override
    public void reiniciarComponente() {
        super.reiniciarComponente();
        brillo_inicial = (display.getBrightness());
        brillo_actual = brillo_inicial;
        barra_progreso.setPorcentaje(pasaporcentaje(brillo_inicial));    }
}
