package com.bowlingspot.hud.menu;



/**
 * 
 */
public class SubmenuJugar extends SubMenu {


    public SubmenuJugar(float posicion_y, int separacion) {
        super(posicion_y, separacion);

        this.addBotonIzquierdo("boton_1jugador");
        this.addBotonDerecho("boton_2jugadores");
    }

    public void jugar1Jugador() {
        terminar(JUGAR_1_JUGADOR);
    }

    public void jugar2Jugadores() {
        terminar(JUGAR_2_JUGADORES);
    }
    
    /**
     * Realiza la acción correspondiente al pulsar el botón de Atras.
     */
    public void seleccionarAtras() {
        terminar();
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);

        if (spot.isleft()&& inicio_estado >= 0.5)
            seleccionarIzquierdo();
        else if (spot.isright()&& inicio_estado >= 0.5)
            seleccionarDerecho();
        else if (inicio_estado >= 3.0f)
            deseleccionar();

        if (spot.isb2())
            seleccionarAtras();
        if (spot.isb1()) {
            if (isDerechoSeleccionado())
                jugar2Jugadores();
            if (isIzquierdoSeleccionado())
                jugar1Jugador();
        }
    }

}
