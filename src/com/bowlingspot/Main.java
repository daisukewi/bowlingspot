package com.bowlingspot;


import com.jme.app.AbstractGame.ConfigShowMode;

/**
 *
 * @author Daniel
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {


        Game juego = new Game();
        juego.setConfigShowMode(ConfigShowMode.ShowIfNoConfig);
        juego.start();
    }

}
