package com.bowlingspot.game;


import com.bowlingspot.ComponentMannager;
import com.bowlingspot.GameComponent3d;
import com.bowlingspot.hud.Mensaje;
import com.jme.curve.BezierCurve;
import com.jme.curve.CurveController;
import com.jme.light.PointLight;
import com.jme.math.FastMath;
import com.jme.math.Quaternion;
import com.jme.math.Vector3f;
import com.jme.renderer.ColorRGBA;
import com.jme.renderer.Renderer;
import com.jme.scene.CameraNode;
import com.jme.scene.Controller;
import com.jme.scene.Node;
import com.jme.scene.Spatial;
import com.jme.scene.state.BlendState;
import com.jme.scene.state.LightState;
import com.jme.scene.state.MaterialState;
import com.jme.system.DisplaySystem;
import com.jme.util.resource.RelativeResourceLocator;
import com.jme.util.resource.ResourceLocatorTool;
import com.jmex.model.ModelFormatException;
import com.jmex.model.ogrexml.MaterialLoader;
import com.jmex.model.ogrexml.OgreLoader;
import com.jmex.model.ogrexml.anim.AnimationChannel;
import com.jmex.model.ogrexml.anim.Bone;
import com.jmex.model.ogrexml.anim.MeshAnimationController;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;


public class Jugador extends GameComponent3d {

    private AnimationChannel lanzar, lanzar2;
    public PrintedScoreBox PSB;
    private Bola bola;
    public Vector3f[] C1,C2,C3,C4,C5;
    private int e_lanza=-2;
    private Bolo[] NBls;
    private Boolean[] Bolos_Tirados;
    private int Jugador;
    private Bone shoulderBone, shoulderBone2;
    private float angle , angle2 = 0f;
    private MeshAnimationController animControl , animControl2;
    private Node PlayMob, PT;
    private Mensaje Minclina,Mlanza,Mlanzamiento,Minclinayacepta,MaceptarB2,MJ,MJu,Mbolos,MB1Fijar,Merror,MaceptarB1;
    private int aux=0 , tirados_en_primera=0,back=-5, back2=-5;
    private boolean sonidostrike=false;
    private float distancia_camara=0;
    private CameraNode cn;
    private CurveController cc;
    private GestorDatosLanzamiento GDL;


    public Jugador(int i , Bolo[] RN) throws IOException, URISyntaxException, ModelFormatException
    {
        super("jugador"+i);
        NBls=RN;
        Jugador=i;
        LightState ls = display.getRenderer().createLightState();
        ls.detachAll();
        final PointLight light = new PointLight();
        light.setDiffuse(ColorRGBA.white);
        light.setSpecular(ColorRGBA.white);
        light.setLocation(new Vector3f(100.0f, 100.0f, 100.0f));
        light.setEnabled(true);
        ls.attach(light);


        GDL = new GestorDatosLanzamiento();
        root_node.setRenderState(ls);
        root_node.updateRenderState();
        PlayMob=new Node();
        PT=new Node();
        PSB= new PrintedScoreBox();
        bola= new Bola();
        Mensajes();
        Cameranodes();
        Bolos_Tirados= new Boolean[10];
        java.util.Arrays.fill(Bolos_Tirados, false);

        this.Mete_Monigote_Trasparente();
        this.meteMonigote();
        setupKekoControl();
        setupKekoControl_T();


        
    }
public void pre()
{this.movidadelacamara(C1,.2f);}
public void inicio()
{  
   if (aux==0)
   {this.T_to_V();
    ComponentMannager.addComponent(this.MJu);
    ComponentMannager.addComponent(this);
    aux++;}
 if (cam.getLocation().equals(new Vector3f(0, 6, 68)))
    {   PSB.set_Transparent();        
        aux=0;
        e_lanza++;}
}


public boolean lanza(int ronda , float tpf)
{  boolean efectuado = false;
         if (e_lanza==-2)
         {inicio();}
        else if (e_lanza==-1)
          {Prepara_Lateral();}
        else if (e_lanza==0)
         {Bucle_Lateral();}
        else if (e_lanza==1)
        {if(this.bajar_brazo()){e_lanza++;}}
        else if (e_lanza==2)
        {Prepara_Lanzamiento();}
        else if (e_lanza==3)
        {Bucle_Lanzamiento(tpf);}
        else if(e_lanza==4)
        {Control_Lanza();}
        else if (e_lanza==5)
        {control_bolos(ronda, 1);}
        else if (e_lanza==6)
        {show_camara_resultados(PSB.Tirados(ronda, 1),1,ronda); }
        else if (e_lanza==7)
        {Control_decisiones(ronda);}
        else if (e_lanza==8)
        {Prepara_Lateral();}
        else if (e_lanza==9)
         { Bucle_Lateral();}
        else if (e_lanza==10)
        {if(this.bajar_brazo()){e_lanza++;}}
        else if (e_lanza==11)
        {Prepara_Lanzamiento();}
        else if (e_lanza==12)
        {Bucle_Lanzamiento(tpf);}
        else if(e_lanza==13)
        {Control_Lanza();}
        else if (e_lanza==14)
        {control_bolos(ronda,2);}
        else if (e_lanza==15)
        {show_camara_resultados(PSB.Tirados(ronda, 2),2,ronda);}
        else if (e_lanza==16)
        {Control_decisiones_2(ronda);}
        else if (e_lanza==17)
        {Prepara_Lateral();}
        else if (e_lanza==18)
         { Bucle_Lateral();}
        else if (e_lanza==19)
        {if(this.bajar_brazo()){e_lanza++;}}
        else if (e_lanza==20)
        {Prepara_Lanzamiento();}
        else if (e_lanza==21)
        {Bucle_Lanzamiento(tpf);}
        else if(e_lanza==22)
        {Control_Lanza();}
        else if (e_lanza==23)
        {control_bolos(ronda, 2);}
        else if (e_lanza==24)
        {show_camara_resultados(PSB.TiradosE(2),2,ronda);}
        else if (e_lanza==25)
        {prepara_bolos(3);}
        else  if (e_lanza==26)
          {Prepara_Lateral();}
        else if (e_lanza==27)
         { Bucle_Lateral();}
        else if (e_lanza==28)
        {if(this.bajar_brazo()){e_lanza++;}}
        else if (e_lanza==29)
        {Prepara_Lanzamiento();}
        else if (e_lanza==30)
        {Bucle_Lanzamiento(tpf);}
        else if(e_lanza==31)
        {Control_Lanza();}
        else if (e_lanza==32)
        {control_bolos(ronda, 3);}
        else if (e_lanza==33)
        {show_camara_resultados(PSB.TiradosE(3),3,ronda); }
        else if (e_lanza==34)
        {fin_ronda();}
 
         
        else if (e_lanza==38)
        {this.Acepta_Error_B1();}
        else if (e_lanza==39)
        {this.Mfallo(-1);}
        else if (e_lanza==40)
        {Acepta_Error_Lanza();}
        else if (e_lanza==50)
        {efectuado=true;
         e_lanza=-2;}

    return efectuado;
}

private void show_camara_resultados(int bt, int tirada,int ronda)
{

   if (tirada==2 && PSB.Spare(ronda))
   {this.M_bolos(11);}
    else {this.M_bolos(bt);}
    this.reset_animacion();
    this.movidadelacamara(C2,.2f);
    PSB.set_Transparent();
    e_lanza++;
    this.sonidostrike=false;
}

private void Control_decisiones_2(int ronda)
{

  if (cam.getLocation().equals(new Vector3f(0,6,68)))
  {ComponentMannager.removeComponent(this.Mbolos);
    if (ronda==10)
    {
        if (PSB.Strike(10))
       {  prepara_bolos(2);
          e_lanza=17;}
      else if (PSB.Spare(10))
      { 
          reset_bolos();
          e_lanza=25;}
      else
      {fin_ronda();}
    }
    else {fin_ronda();}

  }



}
private void prepara_bolos(int tirada)
{
    if (cam.getLocation().equals(new Vector3f(0,6,68)))
    {   if (tirada==2)
        {reset_bolos();}
        else if (PSB.TiradosE(2)==10)
        {reset_bolos();}

  
    ComponentMannager.removeComponent(this.Mbolos);
    e_lanza++;
    }

}
private void reset_bolos()
{
  {java.util.Arrays.fill(Bolos_Tirados, false);
    this.tirados_en_primera=0;
    for (int i=0;i<10;i++)
     {NBls[i].reset();}}

}
private void fin_ronda(){
    ComponentMannager.removeComponent(this);
    root_node.detachAllChildren();
    PSB.ocultar();
    reset_bolos();
    e_lanza=50;
}
private void Control_decisiones(int ronda)
{
    if (cam.getLocation().equals(new Vector3f(0,6,68)))
    { if (!PSB.Strike(ronda))
       {e_lanza++;}
        else {e_lanza=16;}
      ComponentMannager.removeComponent(this.Mbolos);
    }
    

}

private void musica_bola()
{
    if (!GM.exist("bowlingsound.wav"))
     {GM.play("bowlingsound.wav", 6);}
}
private void musica_movimiento()
{
    if (bolo_mueve())
     {
      if (!GM.exist("bowling2.wav") && !sonidostrike)
          {GM.play("bowling2.wav", 6);
           GM.Stop("bowlingsound.wav");
           this.sonidostrike=true;}

    } else {GM.Stop("bowlingsound.wav");}

}


private boolean bolo_mueve()
{ boolean semueve=false;
    for (int i=0; i<10 ;i++)
     { if (!NBls[i].quietosinprecision())
         {semueve=true;}
     }
return semueve;
}


private void control_bolos(int ronda,int tirada)

{  
   if (bola.Give_me_the_ball_D().getLocalTranslation().z < -48)
    {  
       Boolean ba[] = new Boolean[10];
       java.util.Arrays.fill(ba, false);
       Boolean ab[] = new Boolean[10];
       java.util.Arrays.fill(ab, true);
       musica_movimiento();
       for (int i=0;i<10;i++)
        { ba[i]=(NBls[i].quieto());}
       if (Arrays.equals(ba,ab))
        {  PSB.anotar(ronda, tirada, Bolos_Tirados(tirada));
           root_node.detachChild(bola.Give_me_the_ball());
           bola.DynamicToNode();
           this.AddBola();
           e_lanza++;}
   
   } else {musica_bola();}
  if (bola.Give_me_the_ball_D().getLocalTranslation().z > -30)
  { 
    cam.setLocation(new Vector3f(
            cam.getLocation().x,
            cam.getLocation().y,
            bola.Give_me_the_ball_D().getLocalTranslation().z+this.distancia_camara));
  }
}


private int Bolos_Tirados(int tirada)
{int tirado=0;
       Boolean tirados[] = new Boolean[10];
       java.util.Arrays.fill(tirados, false);
       for(int i=0;i<10;i++)
        { if (NBls[i].caido()) {
              Bolos_Tirados[i]=true;
             tirado++;}}
   if (tirada==1) {this.tirados_en_primera=tirado;}
     else{tirado-=tirados_en_primera;}

return tirado;
}






private void Control_Lanza()
{
    if (aux==0)
    {this.animacion();
    aux++;}

    if (GDL.lanzamiento())
     {
        if (this.is_fin_animacion())
         { aux=0; Mfallo(21);}
     }
    else
     {
        if (spot.YtoA()>0 && spot.YtoA()<90)
        { aux=0; Mfallo(21);}
        else {
                if (GDL.is_AL())
                 {Lanza();}
                else
                {aux=0; Mfallo(23);}
             }



     }



}

private void Acepta_Error_Lanza()
{
    if (spot.isb1())
    {GDL.Reset_GDL();
     ComponentMannager.removeComponent(Merror);
     ComponentMannager.removeComponent(MaceptarB1);
     e_lanza=back-2;
     back=-5;
    }



}
private void Mfallo(int i)
//Fallo 20 -> Mala posicion inicial lanzamiento;
//Fallo 21 -> No soltar B1
//Fallo 22 -> Soltar B1 Antes de tiempo
//Fallo 23 -> No armar suficiente el lanzamiento

{
  if (aux==0)
  { GDL.Reset_GDL();
    back=e_lanza;
    e_lanza=39;

    Merror=new Mensaje(Integer.toString(i));
    Merror.setScale(6f, 6f);
    Merror.setPosition(512, 400);
    ComponentMannager.addComponent(Merror);
    ComponentMannager.addComponent(MaceptarB1);
    this.reset_animacion();
     aux++;}

  if (!spot.isb1())
  {e_lanza=40;
   aux=0;}

}


private void Lanza()
{
     this.lanzamiento();
      this.distancia_camara =  (cam.getLocation().z - bola.Give_me_the_ball_D().getLocalTranslation().z);
      aux=0;
      e_lanza++;
      root_node.detachChild(cn);

}
private void Bucle_Lanzamiento(float tpf)
{   UpdateMonigote(tpf);
    if (spot.isb1() && Buen_Inicio())
     {e_lanza++;}
    else if (spot.isb1() && !Buen_Inicio())
     { back2=e_lanza;
       Merror= new Mensaje("20");
       Merror.setScale(6f, 6f);
       Merror.setPosition(512, 400);
       ComponentMannager.addComponent(Merror);
       ComponentMannager.addComponent(MaceptarB1);
       e_lanza=38;
    }


}
private void Acepta_Error_B1()
{
  if (aux==0)
   {
    if (!spot.isb1())
     {aux++;}
   }
  else if (aux==1)
   {if (spot.isb1())
    {aux=0;
     ComponentMannager.removeComponent(Merror);
     ComponentMannager.removeComponent(MaceptarB1);
     e_lanza=back2-1;
     back2=-5;}
  }

}

private boolean Buen_Inicio()
{
    return (spot.YtoA()>75);}


private void Prepara_Lanzamiento()
{
  if (aux==0){
    PSB.set_Transparent();
    ComponentMannager.addComponent(this.Mlanzamiento);
    ComponentMannager.addComponent(this.MaceptarB2);
    aux++;}

  if (spot.isb2())
  { e_lanza++;
    PSB.set_NotTransparent();
    ComponentMannager.removeComponent(this.Mlanzamiento);
    ComponentMannager.removeComponent(this.MaceptarB2);
    aux=0;}
}

private void Bucle_Lateral()
{   
    if (this.posicionlateral())
     {  this.movidadelacamara(C5,.95f);
        ComponentMannager.removeComponent(this.MB1Fijar);
        PSB.set_NotTransparent();
        this.T_to_V();
        e_lanza++;
     }

}
private void Prepara_Lateral()
{


    if (aux==0)

    {PSB.setTransparent();
     ComponentMannager.addComponent(this.Minclina);
     ComponentMannager.addComponent(this.Minclinayacepta);
     aux++;}
    

    if ((spot.isleft()) ||(spot.isright()))
       {aux=0;
        this.V_to_T();
        this.movidadelacamara(C4,.95f);
        ComponentMannager.removeComponent(this.Minclina);
        ComponentMannager.removeComponent(this.Minclinayacepta);
        ComponentMannager.addComponent(this.MB1Fijar);
        e_lanza++;}



}
public void PreparaPresentacion()
{
    
  if (Jugador==1)
  {MJu.setPosition(512, 712);
  PSB.setPosition(512, 600);}
  else
  {MJu.setPosition(512, 460);
   PSB.setPosition(512, 312);}
  ComponentMannager.addComponent(MJu);
  PSB.set_NotTransparent();
  

}
public void RestPresentacion()
{
    PSB.ocultar();
    MJu.setPosition(855, 30);
    ComponentMannager.removeComponent(MJu);
    PSB.setPosition(512, 685);

}
public void showMJ()
{ComponentMannager.addComponent(MJ);}
public void hideMJ()
{ComponentMannager.removeComponent(MJ);}
public void DettachCN()
{root_node.detachChild(cn);}
public void hideMJu()
{ComponentMannager.removeComponent(MJu);
 root_node.detachAllChildren();
System.out.println(root_node.getChildren().size());}


private void Mensajes()
{

    Minclina = new Mensaje("inclinalateral");
    Minclina.setScale(3f, 4f);
    Minclina.setPosition(512, 400);

    Mlanza = new Mensaje("lanza");
    Mlanza.setScale(6f, 6f);
    Mlanza.setPosition(512, 400);



    Mlanzamiento = new Mensaje("lanzamiento");
    Mlanzamiento.setScale(6f, 6f);
    Mlanzamiento.setPosition(512, 400);

    
    
    Minclinayacepta= new Mensaje("inclinayacepta");
    Minclinayacepta.setScale(5f, 4f);
    Minclinayacepta.setPosition(512, 700);



    MaceptarB2= new Mensaje("aceptarb2");
    MaceptarB2.setScale(4f, 4f);
    MaceptarB2.setPosition(500, 170);

    MaceptarB1= new Mensaje("aceptarb1");
    MaceptarB1.setScale(4f, 4f);
    MaceptarB1.setPosition(500, 160);


    MB1Fijar=new Mensaje("b1fijarposicion");
    MB1Fijar.setScale(4f, 4f);
    MB1Fijar.setPosition(512, 230);



    if (Jugador==1)
    {  MJ= new Mensaje("J1");
       MJu= new Mensaje("Ju1");}
    else
    {MJ= new Mensaje("J2");
     MJu= new Mensaje("ju2");
    }

    MJu.setScale(1.5f, 1.5f);
    MJu.setPosition(855, 30);

    MJ.setScale(5f, 5f);
    MJ.setPosition(512, 400);





}


public void M_bolos(int bolosT)
{
    this.Mbolos= new Mensaje(Integer.toString(bolosT));
    Mbolos.setScale(4f, 4f);
    Mbolos.setPosition(512, 400);
    ComponentMannager.addComponent(Mbolos);
}







private void V_to_T()
{
    ((Node)(PlayMob.getChild("Mano"))).detachChild(bola.Give_me_the_ball());
    ((Node)(PT.getChild("Mano"))).attachChild(bola.Give_me_the_ball());
    root_node.detachChild(PlayMob);
    root_node.attachChild(PT);

}

private void T_to_V()
{
    ((Node)(PT.getChild("Mano"))).detachChild(bola.Give_me_the_ball());
    ((Node)(PlayMob.getChild("Mano"))).attachChild(bola.Give_me_the_ball());
    root_node.detachChild(PT);
    root_node.attachChild(PlayMob);

}

public void Add_Jugador()
{ root_node.attachChild(PlayMob);
  ComponentMannager.addComponent(this);
}
public void up()
{ComponentMannager.addComponent(this);}

public void down()
{ComponentMannager.removeComponent(this);


}

public void AddBola()
{ 
 ((Node)(PlayMob.getChild("Mano"))).attachChild(bola.Give_me_the_ball());
}

public void RemoveBola()
{
 ((Node)(PlayMob.getChild("Mano"))).detachChild(bola.Give_me_the_ball());

}
private void setupKekoControl() {

        animControl = (MeshAnimationController) PlayMob.getController(0);
        lanzar = animControl.getAnimationChannel();
        lanzar.addAllBones();
        animControl.setSpeed(1f);
        animControl.setAnimation(lanzar, "Lanzar");
        animControl.setSpeed(0);
        animControl.setRepeatType(MeshAnimationController.RT_CLAMP);
        shoulderBone = animControl.getBone("playmobil:jRShoulder");
        shoulderBone.setUserControl(true);
        Node mano = animControl.getBone("playmobil:jRWirst").getAttachmentsNode();
        mano.setName("Mano");
        PlayMob.attachChild(mano);
        this.angle = 0f;
        shoulderBone.setUserTransforms(Vector3f.ZERO, (new Quaternion().fromAngles(0f, angle, 0f)), Vector3f.UNIT_XYZ);

    }

private void animacion()

{
              
                animControl = (MeshAnimationController) PlayMob.getController(0);
                shoulderBone.setUserControl(false);
                animControl.setSpeed(.8f);
                animControl.setAnimation(lanzar, "Lanzar");

}
private void reset_animacion()
{
                animControl = (MeshAnimationController) PlayMob.getController(0);
                shoulderBone.setUserControl(false);
                animControl.setSpeed(0f);
                animControl.setAnimation(lanzar, "Lanzar");
                shoulderBone.setUserControl(true);
                Quaternion tempRot = new Quaternion();
                tempRot.fromAngles(0f, angle, 0f);
                animControl.setActive(true);
                shoulderBone.setUserControl(true);
                shoulderBone.setUserTransforms(Vector3f.ZERO, tempRot, Vector3f.UNIT_XYZ);



}
private boolean is_fin_animacion()
{ float[] DN = new float[3];
    if (lanzar.getCurTime() >animControl.getAnimationLength("Lanzar"))
    {shoulderBone.setUserControl(true);}
    shoulderBone.getLocalRot().toAngles(DN);
return (lanzar.getCurTime()>animControl.getAnimationLength("Lanzar"));
}

private void lanzamiento()
{
 this.RemoveBola();
 bola.Lanzamiento((-((GDL.get_accX()*10)+3000)),(-GDL.get_accY()));
 GDL.Reset_GDL();
 root_node.attachChild(bola.Give_me_the_ball());
}
public boolean subir_brazo()
        
{
  if (Math.toDegrees(angle) < 90)
  {angle += 0.005;}
 shoulderBone.setUserTransforms(Vector3f.ZERO, (new Quaternion().fromAngles(0f, angle, 0f)), Vector3f.UNIT_XYZ);

 return (Math.toDegrees(angle) > 89);
}

private boolean bajar_brazo()
  {
  if (Math.toDegrees(angle) > 0)
        {angle -= 0.01;}
 shoulderBone.setUserTransforms(Vector3f.ZERO, (new Quaternion().fromAngles(0f, angle, 0f)), Vector3f.UNIT_XYZ);
 return (Math.toDegrees(angle) < 1);
  }


public boolean posicionlateral() {


        if (spot.getIncX()<-45) {

            if (PT.getLocalTranslation().x >= -1.4f) {
                PT.setLocalTranslation(
                        PT.getLocalTranslation().x - 0.02f,
                        PT.getLocalTranslation().y,
                        PT.getLocalTranslation().z);

            }
        } else if (spot.getIncX()>45) {

            if (PT.getLocalTranslation().x <= 1.4f) {
                PT.setLocalTranslation(
                        PT.getLocalTranslation().x + 0.02f,
                        PT.getLocalTranslation().y,
                        PT.getLocalTranslation().z);
            }
        }
PlayMob.setLocalTranslation(PT.getWorldTranslation());
return (spot.isb1());
    }



private void meteMonigote() throws URISyntaxException, IOException, ModelFormatException {

      
        PlayMob = (Node) loadMeshModel(Jugador);
        PlayMob.setName("PM");
        PlayMob.setLocalTranslation(new Vector3f(0f, 0.3f, 55f));
        PlayMob.setLocalScale(1.25f);
        PlayMob.setLocalRotation(new Quaternion().fromAngles(0, (float) (Math.PI / 2), 0));

    }
 private void Mete_Monigote_Trasparente() throws URISyntaxException, IOException, ModelFormatException
 {
        PT = (Node) loadMeshModel_T();
        MaterialState matTranspa;
        matTranspa = display.getRenderer().createMaterialState();
        matTranspa.setAmbient(new ColorRGBA(0.0f, 0.0f, 0.0f, 0.3f));
        matTranspa.setDiffuse(new ColorRGBA(0.1f, 0.5f, 0.8f, 0.3f));
        matTranspa.setSpecular(new ColorRGBA(1.0f, 1.0f, 1.0f, 0.3f));
        matTranspa.setShininess(128.0f);
        matTranspa.setEmissive(new ColorRGBA(0.0f, 0.0f, 0.0f, 0.3f));
        matTranspa.setEnabled(true);
        matTranspa.setMaterialFace(MaterialState.MaterialFace.FrontAndBack);
        PT.setRenderState(matTranspa);
        PT.updateRenderState();
        PT.setName("PM");
        final BlendState alphaState = DisplaySystem.getDisplaySystem().getRenderer().createBlendState();
        alphaState.setBlendEnabled(true);
        alphaState.setSourceFunction(BlendState.SourceFunction.SourceAlpha);
        alphaState.setDestinationFunction(BlendState.DestinationFunction.OneMinusSourceAlpha);
        alphaState.setTestEnabled(true);
        alphaState.setTestFunction(BlendState.TestFunction.GreaterThan);
        alphaState.setEnabled(true);
        matTranspa.setDiffuse(ColorRGBA.black);
        matTranspa.getDiffuse().a = (float) 0.35;
        PT.setRenderState(alphaState);
        PT.updateRenderState();
        PT.setRenderQueueMode(Renderer.QUEUE_TRANSPARENT);
        PT.updateRenderState();
        PT.setLocalTranslation(new Vector3f(0f, 0.3f, 55f));
        PT.setLocalScale(1.25f);
        PT.setLocalRotation(new Quaternion().fromAngles(0, (float) (Math.PI / 2), 0));

 }
 private void UpdateMonigote(float tpf) {

             
    


        Quaternion tempRot = new Quaternion();
        tempRot.fromAngles(0f,(float) Math.toRadians(((spot.AlphaTo360()+90)%360)), 0f);
        animControl = (MeshAnimationController) PlayMob.getController(0);
        animControl.setActive(true);
        shoulderBone.setUserControl(true);      
        shoulderBone.setUserTransforms(Vector3f.ZERO, tempRot, Vector3f.UNIT_XYZ);

    }

private void setupKekoControl_T() {

        animControl2 = (MeshAnimationController) PT.getController(0);
        lanzar2 = animControl2.getAnimationChannel();
        lanzar2.addAllBones();
        animControl2.setSpeed(1f);
        animControl2.setAnimation(lanzar, "Lanzar");
        animControl2.setSpeed(0);
        animControl2.setRepeatType(MeshAnimationController.RT_CLAMP);
        shoulderBone2 = animControl2.getBone("playmobil:jRShoulder");
        shoulderBone2.setUserControl(true);
        Node mano = animControl2.getBone("playmobil:jRWirst").getAttachmentsNode();
        mano.setName("Mano");
        PT.attachChild(mano);
        this.angle2 = (float) Math.toRadians(90);
        shoulderBone2.setUserTransforms(Vector3f.ZERO, (new Quaternion().fromAngles(0f, angle2, 0f)), Vector3f.UNIT_XYZ);

    }
public boolean AC3()
{return (cam.getLocation().equals(new Vector3f(20,12,92)));}
private void  Cameranodes()
{
    C1 =new Vector3f[]{
            new Vector3f(20, 12, 92),
            new Vector3f(18, 11, 88),
            new Vector3f(14, 10, 81),
            new Vector3f(10, 9, 74),
            new Vector3f(6, 8, 72),
            new Vector3f(4, 7, 70),
            new Vector3f(0, 6, 68),};

   C2=new Vector3f[]{
            new Vector3f(0,4,-9.5f),
            new Vector3f(0, 6, 68)};
   C3 =new Vector3f[]{
            new Vector3f(0, 6, 68),
            new Vector3f(2, 6, 53),
            new Vector3f(4, 7, 60),
            new Vector3f(6, 8, 67),
            new Vector3f(10, 9, 74),
            new Vector3f(14, 10, 81),
            new Vector3f(18, 11, 88),
            new Vector3f(20, 12, 92)};
 

   C4 =new Vector3f[]{
            new Vector3f(0,6,68),
            new Vector3f(0,4,64)};

    C5 =new Vector3f[]{
          new Vector3f(0,4,64),
          new Vector3f(0,6,68)
            };
   

}
public void movidadelacamara(Vector3f[] cameraPoints , float speed) {
        root_node.detachChild(cn);
        BezierCurve bc = new BezierCurve("camera path", cameraPoints);
        cn = new CameraNode("cameranode", display.getRenderer().getCamera());
        cc = new CurveController(bc, cn);
        cc.setRepeatType(Controller.RT_CLAMP);
        cc.setSpeed(speed);
        cn.addController(cc);
        cn.lookAt(new Vector3f(0f, 2f, -70f), Vector3f.UNIT_Y);
        cn.setLocalRotation(new Quaternion((float) 0, (float) 1, (float) 0, (float) 0));
        root_node.attachChild(cn);
    }


}
