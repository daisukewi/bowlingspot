package com.bowlingspot.game;

class Turno {


    private int primera, segunda,segundaextra , terceraextra ,parcial;

    public Turno()
    {
        primera=-1;
        segunda=-1;
        parcial=-1;
        segundaextra=-1;
        terceraextra=-1;
    }

   public void set1(int i)
   {primera=i;
    if (i==10)
    {set2(0);}}
   public void set2(int i)
   {segunda=i;}
   public int get1()
   {return primera;}
   public int get2()
   {return segunda;}
   public void setparcial(int i)
   {parcial=i;}
   public int getparcial()
   {return parcial;}
   public boolean strike()
   {return (primera==10 && segunda==0);}
   public boolean spare()
   { return (((primera+segunda)==10)&& (primera!=10));}
   public boolean vacio()
   {return ((primera==-1)&&(segunda==-1));}
   public boolean filled()
   {return ((primera!=-1)&&(segunda!=-1));}
   public boolean semi()
   {return (segunda==-1);}
   public int get2e()
   {return segundaextra;}
   public int get3e()
   {return terceraextra;}
   public void set2e(int i)
   {segundaextra=i;}
   public void set3e(int i)
   {terceraextra=i;}

   public int getsuma()
   {return (primera+segunda);}


}
