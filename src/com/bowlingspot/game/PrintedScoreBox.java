package com.bowlingspot.game;



import com.bowlingspot.ComponentMannager;
import com.bowlingspot.hud.Mensaje;
import com.jme.renderer.ColorRGBA;
import com.jme.scene.Node;
import com.jme.scene.Spatial;
import com.jme.scene.Text;


/**
 *
 */
public class PrintedScoreBox extends Mensaje {

       private  ScoreBox SB;
       private  Node numeros;       
       private int[] posH = {-815 , -725 , -645 ,  -563 , -477 ,  -397 , -309 , -228 , -140 , -50,
                              30 , 109 , 200 , 283 , 362 , 447 , 531 , 613 , 693 , 748 , 802};
       private int []posPar = {-800 , -600 , -465  , -294 , -125 , 45 , 215 , 377 , 546 , 708 , 817};

       private int []posV ={41 ,-30 };


    public PrintedScoreBox ()
    {
        super ("marcadortra");
        this.setScale(1.5f, 2f);
        this.setPosition(512,685);
        SB = new ScoreBox();
        numeros=new Node();
        numeros.setName("Puntos");
        
    }

    public void anotar (int ronda ,int tirada ,int bolos)
    {
        SB.anota((ronda) , bolos);
        actualizascorebox(ronda);
        root_node.detachChild(numeros);
        root_node.attachChild(numeros);
    }

    public void ocultar()
    {ComponentMannager.removeComponent(this);
     root_node.detachChild(numeros);
    }
    public void mostrar()
    {ComponentMannager.addComponent(this);
     root_node.attachChild(numeros);
    }

    public void set_NotTransparent(){
        ComponentMannager.removeComponent(this);
        this.loadImage("marcador");
        ComponentMannager.addComponent(this);
         root_node.detachChild(numeros);
         root_node.attachChild(numeros);
        


    }
     public void set_Transparent(){
        ComponentMannager.removeComponent(this);
        this.loadImage("marcadortra");
        ComponentMannager.addComponent(this);
         root_node.detachChild(numeros);
         root_node.attachChild(numeros);


    }

    public int Tirados(int ronda, int tirada)
    {
        if (tirada==1)
         {return SB.getT1(ronda);}
        else
        { return SB.getT2(ronda);}
    }
    public int TiradosE(int tirada)
    {
        if (tirada==2)
        {return SB.getT2e(10);}
        else
        {return SB.getT3(10);}

    }
    public boolean Strike(int ronda)
    { return SB.strike(ronda);}
    public boolean Spare(int ronda)
    { return SB.spare(ronda);}
    public boolean StrikeE(int ronda)
    {return SB.strikeespecial(ronda);}

   

    private void actualizascorebox(int ronda) {
        //int i=0;
        int tirada=1;
        numeros.detachAllChildren();
        for (int i=1, j=1 ;i<=lanzamientos(ronda);i++)
        {
         String print;
         if (SB.strike(j))
           { if (tirada==1){print="X";} else {print="-";}}
         else if (SB.spare(j) && tirada==2) {
             print="/";
            } else {print=Integer.toString(SB.getSBp(j, tirada));}
         if(print.equals("0")) {print="-";}
         Text t = new Text("T"+i, print);
         t.setLightCombineMode(Spatial.LightCombineMode.Off);
         t.setCullHint(Spatial.CullHint.Never);
         t.setRenderState(Text.getDefaultFontTextureState());
         t.setCastsShadows(true);
         t.setRenderState(Text.getFontBlend());
         t.setLocalScale(2.5f);
         t.setTextColor(ColorRGBA.red);
         t.setLocalTranslation(posH[i-1], posV[0], 0);
         t.updateRenderState();
         numeros.attachChild(t);
         
         
         if (j==10 && tirada==2 && (SB.spare(ronda-1)||SB.strike(ronda-1)))
          {tirada=3;}
         if (SB.par(i) && j<10){j++;}
         if (tirada==1) {tirada++;}
          else if( tirada==2)
          {tirada--;}
        }
        
      
        for (int i=1;i<11;i++)
        {if (SB.vacio(i))
          {
         String st=   Integer.toString(SB.getpuntuaciones(i));
         if (st.equals("-1"))
          {st= ""; }
         Text t = new Text("T"+i, st);
         t.setLightCombineMode(Spatial.LightCombineMode.Off);
         t.setCullHint(Spatial.CullHint.Never);
         t.setRenderState(Text.getDefaultFontTextureState());
         t.setRenderState(Text.getFontBlend());
         t.setLocalScale(2.2f);
         t.setTextColor(ColorRGBA.red);
         t.setLocalTranslation(posPar[i-1], posV[1], 0);
         t.updateRenderState();
         numeros.attachChild(t);
         
         
         
         
         
         }
        }




    }
    private int lanzamientos(int ronda)
    { int lanzamientos=0;

            if (SB.getSBrParcial2(ronda))
             {  lanzamientos=(ronda)*2;}
            else {lanzamientos=((ronda)*2)-1;}

           if (ronda==10)
            {
                if ( SB.strike(9) || SB.spare(9))
                 {  lanzamientos=((ronda)*2+1);}
                else if (SB.getSBrParcial2(9))
                    {lanzamientos=(ronda)*2;}
                     else {lanzamientos=((ronda)*2)-1;}
           }

         //  if(lanzamientos==21) {System.out.println("exito");}

     return lanzamientos;
    }

    public void updateme()
    {
    }


}
