package com.bowlingspot.game;

import com.bowlingspot.ComponentMannager;
import com.bowlingspot.GameComponent;
import com.bowlingspot.GameComponent3d;
import com.jme.bounding.BoundingSphere;
import com.jme.math.Vector3f;
import com.jme.scene.Node;
import com.jme.scene.Spatial;
import com.jme.scene.Spatial.LightCombineMode;
import com.jme.scene.shape.Sphere;
import com.jme.util.export.binary.BinaryImporter;
import com.jmex.model.converters.ObjToJme;
import com.jmex.model.util.ModelLoader;
import com.jmex.physics.DynamicPhysicsNode;
import com.jmex.physics.PhysicsSpace;
import com.jmex.physics.material.Material;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;


/**
 *
 */
public class Bola extends GameComponent3d {


    private DynamicPhysicsNode Bola_din;
    private Node    Bola_Node;
    private Node Bola;
    

    public Bola() throws IOException
    {
        super("Bola");
        Bola=new Node();
        BNode();
        Bdinamica();

    //    root_node.attachChild(Bola);
        ComponentMannager.addComponent(this);
        set_B_Node();

    }

	private void set_B_Node()
	{Bola.attachChild(Bola_Node);
	}
	
	public Node Give_me_the_ball_D()
	{
	    return Bola_din;
	 }
	public Node Give_me_the_ball()
	{
	    return Bola;}
	
	public void DynamicToNode()
	 {
	    Bola.detachChild(Bola_din);
	    Bola_din.setActive(false);
	    Bola_din.setAffectedByGravity(false);
	    Bola.attachChild(Bola_Node);
	
	 }
	
	public void NodeToDynamic()
	{
	    Bola.detachChild(Bola_Node);
	    System.out.println("BN WT "+ Bola_Node.getWorldTranslation());
	    Bola_din.setLocalTranslation(new Vector3f(Bola_Node.getWorldTranslation().x,Bola_Node.getWorldTranslation().y,Bola_Node.getWorldTranslation().z-0.2f));
	    Bola_din.clearDynamics();
	    Bola_din.setActive(true);
	    Bola_din.setAffectedByGravity(true);
	    Bola.attachChild(Bola_din);
	}
	
	public void Lanzamiento(double fuerza, double lateral)
	{
	   NodeToDynamic();
	   lateral=datolateral(lateral);
	   ((DynamicPhysicsNode)(Bola.getChild("Bola_Dinamica"))).clearDynamics();
	   ((DynamicPhysicsNode)(Bola.getChild("Bola_Dinamica"))).addForce(new Vector3f((float)lateral,0,(float)fuerza));

	}
	
	public double datolateral(double lateral)
	{
	   if (lateral>10) {lateral=10;}

	  if (lateral<-10){lateral=-10;}

	  if (lateral < 0 && lateral > -3)
	  {lateral=0;}
	  if (lateral >0 && lateral <3)
	  {lateral=0;}


	  return (lateral*10);
	}
	
	 private void BNode() throws IOException
	 {
	        Bola_Node = new Node();
	        Bola_Node.attachChild(this.loadModelSP("Bola1.obj"));
	        Bola_Node.setName("BolaNodal");
	        Bola_Node.setLightCombineMode(LightCombineMode.Off);
	        Bola_Node.setLocalScale(new Vector3f(.33f, .33f, .33f));
	        Bola_Node.setLocalTranslation(new Vector3f(0.4f, 0, 0));
	 }
	
	 private void Bdinamica() throws IOException {
	        Bola_din = this.physicsspace.createDynamicNode();
	        Bola_din.attachChild(this.loadModelSP("Bola1.obj"));
	        Sphere esfera = new Sphere("Bola fisica", new Vector3f(), 50, 50, 0.9f);
	        Bola_din.attachChild(esfera);
	        Bola_din.setModelBound(new BoundingSphere());
	        Bola_din.updateModelBound();
	        Bola_din.setAffectedByGravity(false);
	        Bola_din.setActive(false);
	        Bola_din.setName("Bola_Dinamica");
	        Bola_din.setLightCombineMode(LightCombineMode.Off);
	        Bola_din.setMaterial(Material.GRANITE);
	        Bola_din.setLocalScale(.45f);
	        Bola_din.generatePhysicsGeometry();
	        Bola_din.updateRenderState();
	        Bola_din.setMaterial(Material.GRANITE);
	        Bola_din.computeMass();
	}

}
