package com.bowlingspot.game;


import com.bowlingspot.ComponentMannager;
import com.bowlingspot.GameComponent;


/**
 *
 * @author Brais
 */
public class GestorDatosLanzamiento extends GameComponent {

    private double accX;
    private double accY;
    private boolean AnguloLimite;


	public GestorDatosLanzamiento()
	{   super("GDL");
	    accX=0;
	    accY=0;
	    AnguloLimite=false;
	    ComponentMannager.addComponent(this);
	}

	public boolean lanzamiento() { 
	    if (AnguloLimite) {
	    	accY += spot.getAccX();
	    	accX += Math.abs(spot.getAccY());
	    } else {
	    	double xx = (spot.AlphaTo360()+90)%360;
	    	if (xx<330 && xx>270) {
	    		AnguloLimite=true;
	    	}
	    }
	    return spot.isb1();
	}
	
	public double get_accX()
	{return accX;}
	public double get_accY()
	{return accY;}
	
	public boolean is_AL()
	{return AnguloLimite;}
	
	public void Reset_GDL()
	{AnguloLimite=false;
	 accX=0;
	 accY=0;}

}