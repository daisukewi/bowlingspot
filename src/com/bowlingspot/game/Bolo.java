package com.bowlingspot.game;


import com.bowlingspot.ComponentMannager;
import com.bowlingspot.GameComponent3d;
import com.jme.bounding.BoundingBox;
import com.jme.light.PointLight;
import com.jme.math.Quaternion;
import com.jme.math.Vector3f;
import com.jme.renderer.ColorRGBA;
import com.jme.scene.state.LightState;
import com.jmex.physics.DynamicPhysicsNode;
import com.jmex.physics.material.Material;


/**
 *
 * @author Brais
 */
public class Bolo extends GameComponent3d {
    
    
    private Vector3f PosicionInicial;
    private DynamicPhysicsNode DPN;
    private int numero;
    
    
    public Bolo(Vector3f v3f, int i)
     {

        super ("Bolo"+i);
         LightState ls = display.getRenderer().createLightState();
        ls.detachAll();
        final PointLight light = new PointLight();
        light.setDiffuse(ColorRGBA.white);
        light.setSpecular(ColorRGBA.white);
        light.setLocation(new Vector3f(100.0f, 100.0f, 100.0f));
        light.setEnabled(true);
        ls.attach(light);

        root_node.setRenderState(ls);
        root_node.updateRenderState();
        numero=i;
        PosicionInicial=new Vector3f(v3f);
        DPN = this.physicsspace.createDynamicNode();

            DPN.setName("Bolo" + i);
            DPN.attachChild(loadModelSP("Bolo.obj"));
            DPN.setMaterial(Material.WOOD);
            DPN.setModelBound(new BoundingBox());
            DPN.updateModelBound();
            DPN.setActive(true);
            DPN.setLocalScale(0.5f);
            DPN.setAffectedByGravity(true);
            DPN.generatePhysicsGeometry();
            DPN.computeMass();
            DPN.setLocalTranslation(PosicionInicial.x,PosicionInicial.y,PosicionInicial.z);
     root_node.attachChild(DPN);
     ComponentMannager.addComponent(this);
    
    }


    public void reset()
    {   root_node.detachChild(DPN);
        ComponentMannager.removeComponent(this);
        DPN.clearDynamics();
        DPN.setLocalRotation(new Quaternion());
        DPN.setLocalTranslation(PosicionInicial.x,PosicionInicial.y,PosicionInicial.z);
        DPN.setAffectedByGravity(true);
        DPN.setActive(true);
        root_node.attachChild(DPN);
        ComponentMannager.addComponent(this);

    }
    public Vector3f Get_Pos()
    {return DPN.getWorldTranslation();}

    private void limpia()
    {   root_node.detachChild(DPN);
        ComponentMannager.removeComponent(this);
        DPN.setActive(false);
        DPN.setAffectedByGravity(false);
        DPN.clearDynamics();
        DPN.setLocalRotation(new Quaternion());
        DPN.setLocalTranslation(new Vector3f(0,0,100));
    }
    private void recoloca()
    {DPN.clearDynamics();
     DPN.setLocalRotation(new Quaternion());
     DPN.setLocalTranslation(PosicionInicial.x,PosicionInicial.y,PosicionInicial.z);

    }


    public boolean caido()
    {
     boolean caido=false;

     if(!DPN.isActive())

     {caido=true;}
     else
      { Vector3f pos=DPN.getLocalTranslation();
        float[] rot=new float[3];
        DPN.getLocalRotation().toAngles(rot);

        if ((pos.x>2)||(pos.x<-2)||pos.z<-48)
         {caido=true;}

       if ((Redondear(rot[0],2)!=0)||(Redondear(rot[2],2)!=0))
        {caido=true;}


        }
     if (caido) {limpia();}
     else {recoloca();}
     return caido;
    }

    public boolean quieto()
    {
        Vector3f v3f = new Vector3f();
                DPN.getLinearVelocity(v3f);
                v3f.setX((float) Redondear(v3f.x, 2));
                v3f.setY((float) Redondear(v3f.y, 2));
                v3f.setZ((float) Redondear(v3f.z, 2));
        return (v3f.equals(Vector3f.ZERO));
    }
    
    
     public boolean quietosinprecision()
     {
        Vector3f v3f = new Vector3f();
                DPN.getLinearVelocity(v3f);
                v3f.setX((float) Redondear(v3f.x, 1));
                v3f.setY((float) Redondear(v3f.y, 1));
                v3f.setZ((float) Redondear(v3f.z, 1));
        return (v3f.equals(Vector3f.ZERO));

     }
    
    
    
   public double Redondear(double nD, int nDec) {

        return Math.round(nD * Math.pow(10, nDec)) / Math.pow(10, nDec);

    }

}
