package com.bowlingspot.game;
/**
 * Representa una ronda donde un jugador tiene 2 tiradas.
 * Aqu� se controla cuantos bolos se han tirado en cada una de esas dos tiradas.
 * @author Daniel
 */
class Ronda {
	private int tiradas[];
	private int puntuacion;
	private boolean usado;
	
	public Ronda () {
		tiradas = new int[2];
		for (int i = 0; i < 2; i++)
			tiradas[i] = -1;
		puntuacion = -1;
		usado = false;
	}
	
	/**
	 * Anota en la tirada correspondiente, el numero de bolos tirados
	 */
	public void anotaTirada (int tirada, int n_bolos) {
		tiradas[tirada] = n_bolos;
		usado = true;
	}
	
	/**
	 * Devuelve la puntuacion almacenada de esta ronda
	 * @return si no se ha calculado todav�a devuelve -1
	 */
	public int getPuntuacion() {
		return this.puntuacion;
	}
	
	/**
	 * pone la puntuacion que le corresponde a esta ronda
	 */
	public void setPuntuacion (int puntuacion) {
		this.puntuacion = puntuacion;
	}
	
	/**
	 * Devuelve true si se ha hecho strike en alguna de las tiradas de la ronda
	 */
	public boolean isStrike() {
		return isStrike(1) || isStrike(2);
	}
	
	/**
	 * Devuelve si se ha hecho strike en una tirada en concreto
	 */
	public boolean isStrike(int tirada) {
		return getNumBolos(tirada) == 10;
	}
	
	/**
	 * Devuelve true si se ha hecho un spare en esta ronda
	 */
	public boolean isSpare() {
		return !isStrike() && (getNumBolos(0) + getNumBolos(1) == 10);
	}
	
	/**
	 * @return true si se ha hecho algun lanzamiento en esta ronda
	 */
	public boolean esUsado() {
		return this.usado;
	}

	public int getNumBolos (int tirada) {
		return tiradas[tirada];
	}
}
