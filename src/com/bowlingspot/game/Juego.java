package com.bowlingspot.game;


import com.bowlingspot.GameComponent3d;
import com.jme.curve.CurveController;
import com.jme.light.PointLight;
import com.jme.math.Vector3f;
import com.jme.renderer.ColorRGBA;
import com.jme.scene.CameraNode;
import com.jme.scene.Node;
import com.jme.scene.Spatial.LightCombineMode;
import com.jme.scene.state.LightState;
import com.jmex.model.ModelFormatException;
import com.jmex.physics.StaticPhysicsNode;
import com.jmex.physics.material.Material;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Carga el escenario y todos los objetos en 3D.
 * Una vez haya terminado da comienzo el juego.
 */
public class Juego extends GameComponent3d {

    private StaticPhysicsNode pista_bolos;
    private Node escenario_bolera;
    private Vector3f[] PosicionBolos;

    private Bolo[] bolos;
    //,,,,,,,,,,,,,,,,,,,,,,,,,,,//
    CameraNode cn;
    CurveController cc;
    //,,,,,,,,,,,,,,,,,,,,,,,,,,,,,//


    private Jugador J1,J2;
	private int num_jugadores;
    private double estado_game = -1;
    //,,,,,,,,,,,,,,,,,,,,,,,,,,,,,//

    public Juego() {
        super("Escenario");
    }
    
    /**
     * Carga la posicion inicial de los bolos.
     */
    private void PosBolos() { 
        this.PosicionBolos = new Vector3f[10];
        this.PosicionBolos[0] = new Vector3f(0.0f, 2f, -46f);

        this.PosicionBolos[1] = new Vector3f(-0.6f, 2f, -46.6f);
        this.PosicionBolos[2] = new Vector3f(0.6f, 2f, -46.6f);

        this.PosicionBolos[3] = new Vector3f(-1.2f, 2f, -47.2f);
        this.PosicionBolos[4] = new Vector3f(0.0f, 2f, -47.2f);
        this.PosicionBolos[5] = new Vector3f(1.2f, 2f, -47.2f);

        this.PosicionBolos[6] = new Vector3f(-1.8f, 2f, -47.8f);
        this.PosicionBolos[7] = new Vector3f(-0.6f, 2f, -47.8f);
        this.PosicionBolos[8] = new Vector3f(0.6f, 2f, -47.8f);
        this.PosicionBolos[9] = new Vector3f(1.8f, 2f, -47.8f);
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);

        if (num_jugadores == 1)
        {Juega1(tpf);}
        else if (num_jugadores == 2)
        {Juegan2(tpf);}
		
    }
	
	private void Juegan2(float tpf) {
	//J1 Ronda 1
		if (estado_game == -1) {
			J1.pre();
			estado_game++;
		} else if (estado_game == 0) {
			J1.Add_Jugador();
			J1.AddBola();
			estado_game++;
		} else if ((estado_game) == 1) {
			if (J1.subir_brazo()) {
				estado_game++;
			}
		} else if (estado_game == 2) {
			if (J1.lanza(1, tpf)) {
				estado_game++;
			}
		} else if (estado_game == 3) {
			J1.PreparaPresentacion();
			J2.PreparaPresentacion();
	  
		   // J2.Print();
			estado_game++;
		} else if (estado_game == 4) {
			if (spot.isb1()) {
				estado_game++;
			   // J2.Print();
				J1.RestPresentacion();
				J2.RestPresentacion();
				J2.showMJ();
				J1.hideMJu();
				J1.movidadelacamara(J1.C3, .5f);
			}
		}
	// J2 Ronda 1
		else if (estado_game == 5) {
			if (J1.AC3()) {
				estado_game++;
				J1.DettachCN();
				J1.down();
			}
		} else if (estado_game == 6) {
			J2.pre();
			estado_game++;
		} else if (estado_game == 7) {
			J2.Add_Jugador();
			J2.AddBola();

			estado_game++;
		} else if ((estado_game) == 8) {
			if (J2.subir_brazo()) {
				J2.hideMJ();

				estado_game++;
			}
		} else if (estado_game == 9) {
			if (J2.lanza(1, tpf)) {
				estado_game++;
			}
		} else if (estado_game == 10) {
			J2.PreparaPresentacion();
			J1.PreparaPresentacion();
			estado_game++;
		} else if (estado_game == 11) {
			if (spot.isb1()) {
				estado_game++;
				J1.RestPresentacion();
				J2.RestPresentacion();
				J1.showMJ();
				J2.hideMJu();
				J2.movidadelacamara(J2.C3, .5f);
			}
		}
	//J1 Ronda 2
		else if (estado_game == 12) {
			if (J2.AC3()) {
				estado_game++;
				J1.DettachCN();
				J2.down();
			}}
			else if (estado_game == 13) {
			J1.pre();
			estado_game++;
		} else if (estado_game == 14) {
			J1.Add_Jugador();
			J1.AddBola();
			estado_game++;
		} else if ((estado_game) == 15) {
			if (J1.subir_brazo()) {
				J1.hideMJ();
				estado_game++;
			}
		} else if (estado_game == 16) {
			if (J1.lanza(2, tpf)) {
				estado_game++;
			}
		} else if (estado_game == 17) {
			J1.PreparaPresentacion();
			J2.PreparaPresentacion();
			estado_game++;
		} else if (estado_game == 18) {
			if (spot.isb1()) {
				estado_game++;
				J1.RestPresentacion();
				J2.RestPresentacion();
				J2.showMJ();
				J1.hideMJu();
				J1.movidadelacamara(J1.C3, .5f);
			}
		}
		
		
	//J2 Ronda 2
	   else if (estado_game == 19) {
			if (J1.AC3()) {
				estado_game++;
				J1.DettachCN();
				J1.down();
			}
		} else if (estado_game == 20) {
			J2.pre();
			estado_game++;
		} else if (estado_game == 21) {
			J2.Add_Jugador();
			J2.AddBola();

			estado_game++;
		} else if ((estado_game) == 22) {
			if (J2.subir_brazo()) {
				J2.hideMJ();

				estado_game++;
			}
		} else if (estado_game == 23) {
			if (J2.lanza(2, tpf)) {
				estado_game++;
			}
		} else if (estado_game == 24) {
			J2.PreparaPresentacion();
			J1.PreparaPresentacion();
			estado_game++;
		} else if (estado_game == 25) {
			if (spot.isb1()) {
				estado_game++;
				J1.RestPresentacion();
				J2.RestPresentacion();
				J1.showMJ();
				J2.hideMJu();
				J2.movidadelacamara(J2.C3, .5f);
			}
		}

	//J1 Ronda 3
		else if (estado_game == 26) {
			if (J2.AC3()) {
				estado_game++;
				J1.DettachCN();
				J2.down();
			}}
			else if (estado_game == 27) {
			J1.pre();
			estado_game++;
		} else if (estado_game == 28) {
			J1.Add_Jugador();
			J1.AddBola();
			estado_game++;
		} else if ((estado_game) == 29) {
			if (J1.subir_brazo()) {
				J1.hideMJ();
				estado_game++;
			}
		} else if (estado_game == 30) {
			if (J1.lanza(3, tpf)) {
				estado_game++;
			}
		} else if (estado_game == 31) {
			J1.PreparaPresentacion();
			J2.PreparaPresentacion();
			estado_game++;
		} else if (estado_game == 32) {
			if (spot.isb1()) {
				estado_game++;
				J1.RestPresentacion();
				J2.RestPresentacion();
				J2.showMJ();
				J1.hideMJu();
				J1.movidadelacamara(J1.C3, .5f);
			}
		}

		//J2 Ronda 3
	   else if (estado_game == 33) {
			if (J1.AC3()) {
				estado_game++;
				J1.DettachCN();
				J1.down();
			}
		} else if (estado_game == 34) {
			J2.pre();
			estado_game++;
		} else if (estado_game == 35) {
			J2.Add_Jugador();
			J2.AddBola();

			estado_game++;
		} else if ((estado_game) == 36) {
			if (J2.subir_brazo()) {
				J2.hideMJ();

				estado_game++;
			}
		} else if (estado_game == 37) {
			if (J2.lanza(3, tpf)) {
				estado_game++;
			}
		} else if (estado_game == 38) {
			J2.PreparaPresentacion();
			J1.PreparaPresentacion();
			estado_game++;
		} else if (estado_game == 39) {
			if (spot.isb1()) {
				estado_game++;
				J1.RestPresentacion();
				J2.RestPresentacion();
				J1.showMJ();
				J2.hideMJu();
				J2.movidadelacamara(J2.C3, .5f);
			}
		}

	//J1 Ronda 4
		else if (estado_game == 40) {
			if (J2.AC3()) {
				estado_game++;
				J1.DettachCN();
				J2.down();
			}}
			else if (estado_game == 41) {
			J1.pre();
			estado_game++;
		} else if (estado_game == 42) {
			J1.Add_Jugador();
			J1.AddBola();
			estado_game++;
		} else if ((estado_game) == 43) {
			if (J1.subir_brazo()) {
				J1.hideMJ();
				estado_game++;
			}
		} else if (estado_game == 44) {
			if (J1.lanza(4, tpf)) {
				estado_game++;
			}
		} else if (estado_game == 45) {
			J1.PreparaPresentacion();
			J2.PreparaPresentacion();
			estado_game++;
		} else if (estado_game == 46) {
			if (spot.isb1()) {
				estado_game++;
				J1.RestPresentacion();
				J2.RestPresentacion();
				J2.showMJ();
				J1.hideMJu();
				J1.movidadelacamara(J1.C3, .5f);
			}
		}
		//J2 Ronda 4
	   else if (estado_game == 47) {
			if (J1.AC3()) {
				estado_game++;
				J1.DettachCN();
				J1.down();
			}
		} else if (estado_game == 48) {
			J2.pre();
			estado_game++;
		} else if (estado_game == 49) {
			J2.Add_Jugador();
			J2.AddBola();

			estado_game++;
		} else if ((estado_game) == 50) {
			if (J2.subir_brazo()) {
				J2.hideMJ();

				estado_game++;
			}
		} else if (estado_game == 51) {
			if (J2.lanza(4, tpf)) {
				estado_game++;
			}
		} else if (estado_game == 52) {
			J2.PreparaPresentacion();
			J1.PreparaPresentacion();
			estado_game++;
		} else if (estado_game == 53) {
			if (spot.isb1()) {
				estado_game++;
				J1.RestPresentacion();
				J2.RestPresentacion();
				J1.showMJ();
				J2.hideMJu();
				J2.movidadelacamara(J2.C3, .5f);
			}
		}

	//J1 Ronda 5
		else if (estado_game == 54) {
			if (J2.AC3()) {
				estado_game++;
				J1.DettachCN();
				J2.down();
			}}
			else if (estado_game == 55) {
			J1.pre();
			estado_game++;
		} else if (estado_game == 56) {
			J1.Add_Jugador();
			J1.AddBola();
			estado_game++;
		} else if ((estado_game) == 57) {
			if (J1.subir_brazo()) {
				J1.hideMJ();
				estado_game++;
			}
		} else if (estado_game == 58) {
			if (J1.lanza(5, tpf)) {
				estado_game++;
			}
		} else if (estado_game == 59) {
			J1.PreparaPresentacion();
			J2.PreparaPresentacion();
			estado_game++;
		} else if (estado_game == 60) {
			if (spot.isb1()) {
				estado_game++;
				J1.RestPresentacion();
				J2.RestPresentacion();
				J2.showMJ();
				J1.hideMJu();
				J1.movidadelacamara(J1.C3, .5f);
			}
		}
		//J2 Ronda 5
	   else if (estado_game == 61) {
			if (J1.AC3()) {
				estado_game++;
				J1.DettachCN();
				J1.down();
			}
		} else if (estado_game == 62) {
			J2.pre();
			estado_game++;
		} else if (estado_game == 63) {
			J2.Add_Jugador();
			J2.AddBola();

			estado_game++;
		} else if ((estado_game) == 64) {
			if (J2.subir_brazo()) {
				J2.hideMJ();

				estado_game++;
			}
		} else if (estado_game == 65) {
			if (J2.lanza(5, tpf)) {
				estado_game++;
			}
		} else if (estado_game == 66) {
			J2.PreparaPresentacion();
			J1.PreparaPresentacion();
			estado_game++;
		} else if (estado_game == 67) {
			if (spot.isb1()) {
				estado_game++;
				J1.RestPresentacion();
				J2.RestPresentacion();
				J1.showMJ();
				J2.hideMJu();
				J2.movidadelacamara(J2.C3, .5f);
			}
		}

	//J1 Ronda 6
		else if (estado_game == 68) {
			if (J2.AC3()) {
				estado_game++;
				J1.DettachCN();
				J2.down();
			}}
			else if (estado_game == 69) {
			J1.pre();
			estado_game++;
		} else if (estado_game == 70) {
			J1.Add_Jugador();
			J1.AddBola();
			estado_game++;
		} else if ((estado_game) == 71) {
			if (J1.subir_brazo()) {
				J1.hideMJ();
				estado_game++;
			}
		} else if (estado_game == 72) {
			if (J1.lanza(6, tpf)) {
				estado_game++;
			}
		} else if (estado_game == 73) {
			J1.PreparaPresentacion();
			J2.PreparaPresentacion();
			estado_game++;
		} else if (estado_game == 74) {
			if (spot.isb1()) {
				estado_game++;
				J1.RestPresentacion();
				J2.RestPresentacion();
				J2.showMJ();
				J1.hideMJu();
				J1.movidadelacamara(J1.C3, .5f);
			}
		}

		//J2 Ronda 6
	   else if (estado_game == 75) {
			if (J1.AC3()) {
				estado_game++;
				J1.DettachCN();
				J1.down();
			}
		} else if (estado_game == 76) {
			J2.pre();
			estado_game++;
		} else if (estado_game == 77) {
			J2.Add_Jugador();
			J2.AddBola();

			estado_game++;
		} else if ((estado_game) == 78) {
			if (J2.subir_brazo()) {
				J2.hideMJ();

				estado_game++;
			}
		} else if (estado_game == 79) {
			if (J2.lanza(6, tpf)) {
				estado_game++;
			}
		} else if (estado_game == 80) {
			J2.PreparaPresentacion();
			J1.PreparaPresentacion();
			estado_game++;
		} else if (estado_game == 81) {
			if (spot.isb1()) {
				estado_game++;
				J1.RestPresentacion();
				J2.RestPresentacion();
				J1.showMJ();
				J2.hideMJu();
				J2.movidadelacamara(J2.C3, .5f);
			}
		}

	//J1 Ronda 7
		else if (estado_game == 82) {
			if (J2.AC3()) {
				estado_game++;
				J1.DettachCN();
				J2.down();
			}}
			else if (estado_game == 83) {
			J1.pre();
			estado_game++;
		} else if (estado_game == 84) {
			J1.Add_Jugador();
			J1.AddBola();
			estado_game++;
		} else if ((estado_game) == 85) {
			if (J1.subir_brazo()) {
				J1.hideMJ();
				estado_game++;
			}
		} else if (estado_game == 86) {
			if (J1.lanza(7, tpf)) {
				estado_game++;
			}
		} else if (estado_game == 87) {
			J1.PreparaPresentacion();
			J2.PreparaPresentacion();
			estado_game++;
		} else if (estado_game == 88) {
			if (spot.isb1()) {
				estado_game++;
				J1.RestPresentacion();
				J2.RestPresentacion();
				J2.showMJ();
				J1.hideMJu();
				J1.movidadelacamara(J1.C3, .5f);
			}
		}
		//J2 Ronda 7
	   else if (estado_game == 89) {
			if (J1.AC3()) {
				estado_game++;
				J1.DettachCN();
				J1.down();
			}
		} else if (estado_game == 90) {
			J2.pre();
			estado_game++;
		} else if (estado_game == 91) {
			J2.Add_Jugador();
			J2.AddBola();

			estado_game++;
		} else if ((estado_game) == 92) {
			if (J2.subir_brazo()) {
				J2.hideMJ();

				estado_game++;
			}
		} else if (estado_game == 93) {
			if (J2.lanza(7, tpf)) {
				estado_game++;
			}
		} else if (estado_game == 94) {
			J2.PreparaPresentacion();
			J1.PreparaPresentacion();
			estado_game++;
		} else if (estado_game == 95) {
			if (spot.isb1()) {
				estado_game++;
				J1.RestPresentacion();
				J2.RestPresentacion();
				J1.showMJ();
				J2.hideMJu();
				J2.movidadelacamara(J2.C3, .5f);
			}
		}

	//J1 Ronda 8
		else if (estado_game == 96) {
			if (J2.AC3()) {
				estado_game++;
				J1.DettachCN();
				J2.down();
			}}
			else if (estado_game == 97) {
			J1.pre();
			estado_game++;
		} else if (estado_game == 98) {
			J1.Add_Jugador();
			J1.AddBola();
			estado_game++;
		} else if ((estado_game) == 99) {
			if (J1.subir_brazo()) {
				J1.hideMJ();
				estado_game++;
			}
		} else if (estado_game == 100) {
			if (J1.lanza(8, tpf)) {
				estado_game++;
			}
		} else if (estado_game == 101) {
			J1.PreparaPresentacion();
			J2.PreparaPresentacion();
			estado_game++;
		} else if (estado_game == 102) {
			if (spot.isb1()) {
				estado_game++;
				J1.RestPresentacion();
				J2.RestPresentacion();
				J2.showMJ();
				J1.hideMJu();
				J1.movidadelacamara(J1.C3, .5f);
			}
		}
		//J2 Ronda 8
	   else if (estado_game == 103) {
			if (J1.AC3()) {
				estado_game++;
				J1.DettachCN();
				J1.down();
			}
		} else if (estado_game == 104) {
			J2.pre();
			estado_game++;
		} else if (estado_game == 105) {
			J2.Add_Jugador();
			J2.AddBola();

			estado_game++;
		} else if ((estado_game) == 106) {
			if (J2.subir_brazo()) {
				J2.hideMJ();

				estado_game++;
			}
		} else if (estado_game == 107) {
			if (J2.lanza(8, tpf)) {
				estado_game++;
			}
		} else if (estado_game == 108) {
			J2.PreparaPresentacion();
			J1.PreparaPresentacion();
			estado_game++;
		} else if (estado_game == 109) {
			if (spot.isb1()) {
				estado_game++;
				J1.RestPresentacion();
				J2.RestPresentacion();
				J1.showMJ();
				J2.hideMJu();
				J2.movidadelacamara(J2.C3, .5f);
			}
		}

	//J1 Ronda 9
		else if (estado_game == 110) {
			if (J2.AC3()) {
				estado_game++;
				J1.DettachCN();
				J2.down();
			}}
			else if (estado_game == 111) {
			J1.pre();
			estado_game++;
		} else if (estado_game == 112) {
			J1.Add_Jugador();
			J1.AddBola();
			estado_game++;
		} else if ((estado_game) == 113) {
			if (J1.subir_brazo()) {
				J1.hideMJ();
				estado_game++;
			}
		} else if (estado_game == 114) {
			if (J1.lanza(9, tpf)) {
				estado_game++;
			}
		} else if (estado_game == 115) {
			J1.PreparaPresentacion();
			J2.PreparaPresentacion();
			estado_game++;
		} else if (estado_game == 116) {
			if (spot.isb1()) {
				estado_game++;
				J1.RestPresentacion();
				J2.RestPresentacion();
				J2.showMJ();
				J1.hideMJu();
				J1.movidadelacamara(J1.C3, .5f);
			}
		}
		//J2 Ronda 9
	   else if (estado_game == 117) {
			if (J1.AC3()) {
				estado_game++;
				J1.DettachCN();
				J1.down();
			}
		} else if (estado_game == 118) {
			J2.pre();
			estado_game++;
		} else if (estado_game == 119) {
			J2.Add_Jugador();
			J2.AddBola();

			estado_game++;
		} else if ((estado_game) == 120) {
			if (J2.subir_brazo()) {
				J2.hideMJ();

				estado_game++;
			}
		} else if (estado_game == 121) {
			if (J2.lanza(9, tpf)) {
				estado_game++;
			}
		} else if (estado_game == 122) {
			J2.PreparaPresentacion();
			J1.PreparaPresentacion();
			estado_game++;
		} else if (estado_game == 123) {
			if (spot.isb1()) {
				estado_game++;
				J1.RestPresentacion();
				J2.RestPresentacion();
				J1.showMJ();
				J2.hideMJu();
				J2.movidadelacamara(J2.C3, .5f);
			}
		}
	   //J1 Ronda 10
		else if (estado_game == 124) {
			if (J2.AC3()) {
				estado_game++;
				J1.DettachCN();
				J2.down();
			}}
			else if (estado_game == 125) {
			J1.pre();
			estado_game++;
		} else if (estado_game == 126) {
			J1.Add_Jugador();
			J1.AddBola();
			estado_game++;
		} else if ((estado_game) == 127) {
			if (J1.subir_brazo()) {
				J1.hideMJ();
				estado_game++;
			}
		} else if (estado_game == 128) {
			if (J1.lanza(10, tpf)) {
				estado_game++;
			}
		} else if (estado_game == 129) {
			J1.PreparaPresentacion();
			J2.PreparaPresentacion();
			estado_game++;
		} else if (estado_game == 130) {
			if (spot.isb1()) {
				estado_game++;
				J1.RestPresentacion();
				J2.RestPresentacion();
				J2.showMJ();
				J1.hideMJu();
				J1.movidadelacamara(J1.C3, .5f);
			}
		}
		//J2 Ronda 10
	   else if (estado_game == 131) {
			if (J1.AC3()) {
				estado_game++;
				J1.DettachCN();
				J1.down();

			}
		} else if (estado_game == 132) {
			J2.pre();
			estado_game++;
		} else if (estado_game == 133) {
			J2.Add_Jugador();
			J2.AddBola();

			estado_game++;
		} else if ((estado_game) == 134) {
			if (J2.subir_brazo()) {
				J2.hideMJ();

				estado_game++;
			}
		} else if (estado_game == 135) {
			if (J2.lanza(10, tpf)) {
				estado_game++;
			}
		} else if (estado_game == 136) {
			J2.PreparaPresentacion();
			J1.PreparaPresentacion();
			estado_game++;
		} else if (estado_game == 137) {
			if (spot.isb1()) {
				estado_game++;
				J1.RestPresentacion();
				J2.RestPresentacion();
				J1.showMJ();
				J2.hideMJu();
				J2.movidadelacamara(J2.C3, .5f);
			}
		}
	}
	
	private void Juega1(float tpf) {

     if (estado_game==-1)
        { J1.pre();
         estado_game++;}
        if (estado_game==0)
        {J1.Add_Jugador();
         J1.AddBola();
         estado_game++;}
        if ((estado_game)==1)
        {if (J1.subir_brazo())
            {estado_game++;}
        }

    //////////Lanzamiento 1/////////////////
      if (estado_game==2)
        {if (J1.lanza(1,tpf))
         {estado_game++;}
        } else if (estado_game==3)
        { J1.PreparaPresentacion();
          estado_game++;}
         else if  (estado_game==4)
          {if (spot.isb1())
           {estado_game++;
             J1.RestPresentacion();
             J1.showMJ();
           }
          }
     //////////Lanzamiento 2/////////////////
       if ((estado_game)==5)
        {if (J1.subir_brazo())
            {estado_game++;
             J1.hideMJ();}
        }
        if (estado_game==6)
        {if (J1.lanza(2,tpf))
         {estado_game++;}
        } else if (estado_game==7)
        { J1.PreparaPresentacion();
          estado_game++;}
         else if  (estado_game==8)
          {if (spot.isb1())
           {estado_game++;
             J1.RestPresentacion();
             J1.showMJ();
           }
          }
     //////////Lanzamiento 3/////////////////
     if ((estado_game)==9)
        {if (J1.subir_brazo())
            {estado_game++;
                J1.hideMJ();}
        }
        if (estado_game==10)
        {if (J1.lanza(3,tpf))
         {estado_game++;}
        } else if (estado_game==11)
        { J1.PreparaPresentacion();
          estado_game++;}
         else if  (estado_game==12)
          {if (spot.isb1())
           {estado_game++;
             J1.RestPresentacion();
             J1.showMJ();
           }
          }
     //////////Lanzamiento 4/////////////////
     if ((estado_game)==13)
        {if (J1.subir_brazo())
            {estado_game++;
         J1.hideMJ();}
        }
        if (estado_game==14)
        {if (J1.lanza(4,tpf))
         {estado_game++;}
        } else if (estado_game==15)
        { J1.PreparaPresentacion();
          estado_game++;}
         else if  (estado_game==16)
          {if (spot.isb1())
           {estado_game++;
             J1.RestPresentacion();
             J1.showMJ();
           }
          }
     //////////Lanzamiento 5/////////////////
     if ((estado_game)==17)
        {if (J1.subir_brazo())
            {estado_game++;
         J1.hideMJ();}
        }
        if (estado_game==18)
        {if (J1.lanza(5,tpf))
         {estado_game++;}
        } else if (estado_game==19)
        { J1.PreparaPresentacion();
          estado_game++;}
         else if  (estado_game==20)
          {if (spot.isb1())
           {estado_game++;
             J1.RestPresentacion();
             J1.showMJ();
           }
          }
     //////////Lanzamiento 6/////////////////
     if ((estado_game)==21)
        {if (J1.subir_brazo())
            {estado_game++;
         J1.hideMJ();}
        }
        if (estado_game==22)
        {if (J1.lanza(6,tpf))
         {estado_game++;}
        } else if (estado_game==23)
        { J1.PreparaPresentacion();
          estado_game++;}
         else if  (estado_game==24)
          {if (spot.isb1())
           {estado_game++;
             J1.RestPresentacion();
             J1.showMJ();
           }
          }
     //////////Lanzamiento 7/////////////////
     if ((estado_game)==25)
        {if (J1.subir_brazo())
            {estado_game++;
         J1.hideMJ();}
        }
        if (estado_game==26)
        {if (J1.lanza(7,tpf))
         {estado_game++;}
        } else if (estado_game==27)
        { J1.PreparaPresentacion();
          estado_game++;}
         else if  (estado_game==28)
          {if (spot.isb1())
           {estado_game++;
             J1.RestPresentacion();
             J1.showMJ();
           }
          }
     //////////Lanzamiento 8/////////////////
     if ((estado_game)==29)
        {if (J1.subir_brazo())
            {estado_game++;
         J1.hideMJ();}
        }
        if (estado_game==30)
        {if (J1.lanza(8,tpf))
         {estado_game++;}
        } else if (estado_game==31)
        { J1.PreparaPresentacion();
          estado_game++;}
         else if  (estado_game==32)
          {if (spot.isb1())
           {estado_game++;
             J1.RestPresentacion();
             J1.showMJ();
           }
          }
     //////////Lanzamiento 9/////////////////
     if ((estado_game)==33)
        {if (J1.subir_brazo())
            {estado_game++;
         J1.hideMJ();}
        }
        if (estado_game==34)
        {if (J1.lanza(9,tpf))
         {estado_game++;}
        } else if (estado_game==35)
        { J1.PreparaPresentacion();
          estado_game++;}
         else if  (estado_game==36)
          {if (spot.isb1())
           {estado_game++;
             J1.RestPresentacion();
             J1.showMJ();
           }
          }

    //////////Lanzamiento 10/////////////////
     if ((estado_game)==37)
        {if (J1.subir_brazo())
            {estado_game++;
         J1.hideMJ();}
        }
        if (estado_game==38)
        {if (J1.lanza(10,tpf))
         {estado_game++;}
        } else if (estado_game==39)
        { J1.PreparaPresentacion();
          estado_game++;}
         else if  (estado_game==40)
          {if (spot.isb1())
           {estado_game++;
             J1.RestPresentacion();
           }
          }
	}
    
    public void load(int num_jugadores) throws IOException {
		this.num_jugadores = num_jugadores;
    	PosBolos();
        LightState ls = display.getRenderer().createLightState();
        ls.detachAll();
        final PointLight light = new PointLight();
        light.setDiffuse(ColorRGBA.white);
        light.setSpecular(ColorRGBA.white);
        light.setLocation(new Vector3f(100.0f, 100.0f, 100.0f));
        light.setEnabled(true);
        ls.attach(light);

        root_node.setRenderState(ls);
        root_node.updateRenderState();


        cam.setLocation(new Vector3f(0, 10f, 160f));

        try {
            //*********************************************//
            Cargatodo(num_jugadores);
            //*******************************************//
        } catch (URISyntaxException ex) {
            Logger.getLogger(Juego.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ModelFormatException ex) {
            Logger.getLogger(Juego.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    private void cargaEscenarioFisico() {
        pista_bolos = this.physicsspace.createStaticNode();
        StaticPhysicsNode EscenarioFisico = this.physicsspace.createStaticNode();
        EscenarioFisico.setName("EscenarioFisico");
        EscenarioFisico.attachChild(loadModelSP("pista_bolos.obj"));
        EscenarioFisico.setLocalScale(5f);
        EscenarioFisico.setMaterial(Material.ICE);
        EscenarioFisico.generatePhysicsGeometry(true);
        EscenarioFisico.setActive(true);
        EscenarioFisico.setLightCombineMode(LightCombineMode.Off);
        pista_bolos.attachChild(EscenarioFisico);
        pista_bolos.generatePhysicsGeometry(true);
    }

    private void cargaEscenarioNodo() {
        escenario_bolera = new Node();
        escenario_bolera.setName("NodoBoleraObjetos");
        escenario_bolera.attachChild(loadModelSP("objetos.obj"));
        escenario_bolera.getChild(0).setLocalScale(5f);
        escenario_bolera.getChild(0).setName("Objetos");
        escenario_bolera.attachChild(loadModelSP("bolera.obj"));
        escenario_bolera.getChild(1).setName("EscenarioNodo");
        escenario_bolera.getChild(1).setLocalScale(5f);
        escenario_bolera.setLightCombineMode(LightCombineMode.Off);
    }
    
    private void cargaBolos() {
        bolos = new Bolo[10];
        for (int i = 0; i < 10; i++) {
        	bolos[i] = new Bolo(this.PosicionBolos[i], i);
        }
    }

    private void cargaJugadores(int num_jugadores) throws IOException, URISyntaxException, ModelFormatException  {
        J1 = new Jugador(2, this.bolos);
        if (num_jugadores == JUGAR_2_JUGADORES)
        	J2 = new Jugador(2,this.bolos);
    }

    private void Cargatodo(int num_jugadores) throws URISyntaxException, IOException, ModelFormatException {
        this.cargaEscenarioFisico();
        this.cargaEscenarioNodo();
        this.cargaBolos();
        this.cargaJugadores(num_jugadores);

        root_node.attachChild(pista_bolos);
        root_node.attachChild(escenario_bolera);
        root_node.updateRenderState();
        cam.setLocation(new Vector3f(0f, 6f, 150f));
       // movidadelacamara();
    }
	
	private double Redondear(double nD, int nDec) {

        return Math.round(nD * Math.pow(10, nDec)) / Math.pow(10, nDec);

    }

}
