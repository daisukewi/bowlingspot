package com.bowlingspot.game;


public class ScoreBox {

    private Turno[] scorebox;
    private int [] puntuaciones;
    private int[] parciales;


    public ScoreBox() {
    	scorebox = new Turno[10];
        for (int i = 0; i < 10; i++) {
        	scorebox[i] = new Turno();
        }
        parciales = new int[10];
        java.util.Arrays.fill(parciales, -1);
        puntuaciones = new int[10];
        java.util.Arrays.fill(parciales, -5);
       
    }

    public boolean par(int i) {
    	return (i % 2) == 0;
    }

    public int getpuntuaciones(int ronda) {
    	return puntuaciones[ronda-1];
    }

    public int getT1(int ronda) {
    	return scorebox[ronda-1].get1();
    }
  public int getT2(int ronda)
  {return scorebox[ronda-1].get2();}
  public int getT2e(int ronda)
  {return scorebox[ronda-1].get2e();}
  public int getT3(int ronda)
  {return scorebox[ronda-1].get3e();}
  public boolean getSBr(int ronda)
   {return scorebox[ronda-1].vacio();}
  public boolean getSBrParcial2(int ronda)
  {return (scorebox[ronda-1].get2()!=-1);}
  public boolean getSBrParcial2e(int ronda)
  {return (scorebox[ronda-1].get2e()!=-1);}
  public boolean strike(int ronda)
  { 
      return scorebox[ronda-1].strike();}
  public boolean spare (int ronda)
  { 
      return scorebox[ronda-1].spare();}
  
  public boolean strikeespecial(int ronda)
  {return(scorebox[ronda-1].get2e()==10);}
  
  public int getSBp(int ronda,int pos)
  {
        if (pos==1)
         {return scorebox[ronda-1].get1();}
        else if (pos==2) {
            if (scorebox[ronda-1].strike()&& (ronda==9))
            { return scorebox[ronda-1].get2e();}
            else return scorebox[ronda-1].get2();}
        else {return scorebox[ronda-1].get3e();}
  }

  public boolean vacio(int ronda)
  {  return scorebox[ronda-1].filled();}
  

  public void anota(int ronda ,int bolostirados)
    {
      if ((ronda==10)&& (!scorebox[ronda-1].vacio()) && (scorebox[ronda-1].strike() || scorebox[ronda-1].spare()))
      {
          if (scorebox[ronda-1].strike() && (scorebox[ronda-1].get2e()==-1))
           {scorebox[ronda-1].set2e(bolostirados);}

          else if (scorebox[ronda-1].strike() && (scorebox[ronda-1].get2e()!=-1))
          { scorebox[ronda-1].set3e(bolostirados);}

          else if (scorebox[ronda-1].spare())
           { scorebox[ronda-1].set3e(bolostirados);}

      }

      else
      {
       if (scorebox[ronda-1].vacio())
          {scorebox[ronda-1].set1(bolostirados);}
         else {scorebox[ronda-1].set2(bolostirados);}
      }

     parciales();
    }


 public void showscorebox()
  {

    for (int i=0;i<10;i++)
       {System.out.println((i+1)+"º: "+ puntuaciones[i]);}

 }

    private void parciales() {

      int index=0;
      while (index<10 && !scorebox[index].vacio())
      {parciales[index]=scorebox[index].getsuma();
      index++;}
     index=0;
     while (index<8 && !scorebox[index].vacio() )
     {
        if (scorebox[index].strike())
         {
            if (!scorebox[index+1].vacio())
             {  if (scorebox[index+1].strike())
                 {
                    if (!scorebox[index+2].vacio())
                     {if (scorebox[index+2].strike())
                         {parciales[index]+=scorebox[index+2].getsuma();}
                        else {parciales[index]+=scorebox[index+2].get1();}}
                 parciales[index]+=scorebox[index+1].getsuma();
                }
                else
                 {parciales[index]+=scorebox[index+1].getsuma();}
            }
        }
         else if(scorebox[index].spare())
           {  if (!scorebox[index+1].vacio())
             {  if (scorebox[index+1].strike())
                 { parciales[index]+=scorebox[index+1].getsuma();
                } else {parciales[index]+=scorebox[index+1].get1();}
             }


         }


         index++;
     }

     if (!scorebox[8].vacio())
      { if (scorebox[8].strike())
        {  if (!scorebox[9].vacio())
            {if (scorebox[9].strike())
              { if (scorebox[9].get2e()==10)
                  { parciales[8]+=20;}
             } else {parciales[8]+=scorebox[9].getsuma();}
             }

        } else if(scorebox[8].spare())
          {  if (!scorebox[9].vacio())
              {parciales[8]+=scorebox[9].get1();}}
     }

     if (!scorebox[9].vacio())
      {if (scorebox[9].strike())
        { if (scorebox[9].get2e()==10)
           {parciales[9]+=10 + scorebox[9].get3e();}
          else {parciales[9]+=scorebox[9].get2e()+scorebox[9].get3e();}}
        else if(scorebox[9].spare())
          {parciales[9]+=scorebox[9].get3e();}
   }

     parcializa();
     puntuaciones();
    }

    public int vacios()
    { int va=0;

      for (int i=0;i<scorebox.length;i++)
      {
        if (scorebox[i].vacio())
        {va++;}
      }


    return va;
    }
    private void parcializa()
    {
        for (int i=1; i<parciales.length;i++)
        {parciales[i]+=parciales[i-1];}

    }
     public void showparciales()
     {
       // parciales();
        for (int i=0; i<8;i++)
        {
            if (!scorebox[i].vacio())
            {
            if (scorebox[i].strike())
             {if(scorebox[i+1].strike())
                {
                   if(!scorebox[i+2].vacio())
                   {System.out.println((i+1)+"º "+parciales[i]);}
                   else
                   {System.out.println((i+1)+"º "); }
                }
               else if (!scorebox[i+1].semi())
                {System.out.println((i+1)+"º "+parciales[i]);}
               else {System.out.println((i+1)+"º ");}}
            else if (scorebox[i].spare())
             { if(!scorebox[i+1].vacio())
               {
                 System.out.println((i+1)+"º "+parciales[i]);
               } else {System.out.println((i+1)+"º ");}

            }
            else{System.out.println((i+1)+"º "+parciales[i]);}


        } else{System.out.println((i+1)+"º ");}

        }

        if (!scorebox[8].vacio())
          {if(scorebox[8].strike())
            {if (!scorebox[9].vacio())
             { if (scorebox[9].get2() != scorebox[9].get2e())
                {System.out.println(9 + "º "+parciales[8]);}
             } else {System.out.println(9 + "º ");}
           } else if(scorebox[8].spare())
            { if (!scorebox[9].vacio())
               {System.out.println(9 + "º "+parciales[8]); }
              else {System.out.println(9 + "º ");}
            } else {
                    System.out.println(9 + "º "+parciales[8]);
                   }
        }else {System.out.println(9 + "º ");}

        if (!scorebox[9].vacio())
         {
            if (scorebox[9].strike())
             {if(   (scorebox[9].get2e()!=1)&&(scorebox[9].get3e()!=-1))
               {System.out.println(10 + "º "+parciales[9]);     }
               else {System.out.println(10 + "º ");}
             }
            else if( scorebox[9].spare())
             { if (scorebox[9].get3e()!=-1)
               {System.out.println(10 + "º "+parciales[9]);}
               else {System.out.println(10 + "º ");}
            }
            else {System.out.println(10 + "º "+parciales[9]);}
        }
        else{System.out.println(10 + "º "+parciales[9]);}


        puntuaciones();




     }

     private void puntuaciones()
     {

        for (int i=0; i<8;i++)
        {
            if (!scorebox[i].vacio())
            {
            if (scorebox[i].strike())
             {if(scorebox[i+1].strike())
                {
                   if(!scorebox[i+2].vacio())
                   {puntuaciones[i]= parciales[i];}
                   else
                   {puntuaciones[i]=-1; }
                }
               else if (!scorebox[i+1].semi())
                {puntuaciones[i]= parciales[i];}
               else {puntuaciones[i]=-1;}}
            else if (scorebox[i].spare())
             { if(!scorebox[i+1].vacio())
               {
                 puntuaciones[i]= parciales[i];
               } else {puntuaciones[i]=-1;}

            }
            else{puntuaciones[i]= parciales[i];}


        } else{puntuaciones[i]=-1;}

        }

        if (!scorebox[8].vacio())
          {if(scorebox[8].strike())
            {if (!scorebox[9].vacio())
             { if (scorebox[9].get2() != scorebox[9].get2e())
                {puntuaciones[8]= parciales[8];}
             } else {puntuaciones[8]=-1;}
           } else if(scorebox[8].spare())
            { if (!scorebox[9].vacio())
               {puntuaciones[8]= parciales[8]; }
              else {puntuaciones[8]=-1;}
            } else {
                    puntuaciones[8]= parciales[8];
                   }
        }else {puntuaciones[8]=-1;}

        if (!scorebox[9].vacio())
         {
            if (scorebox[9].strike())
             {if(   (scorebox[9].get2e()!=1)&&(scorebox[9].get3e()!=-1))
               {puntuaciones[9]= parciales[9];     }
               else {puntuaciones[9]=-1;}
             }
            else if( scorebox[9].spare())
             { if (scorebox[9].get3e()!=-1)
               {puntuaciones[9]= parciales[9];}
               else {puntuaciones[9]=-1;}
            }
            else {puntuaciones[9]= parciales[9];}
        }
        else{puntuaciones[9]= -1;}

     }

}
