package com.bowlingspot;



import com.bowlingspot.controller.Datos;
import com.bowlingspot.media.GestorMusica;
import com.jme.renderer.Camera;
import com.jme.scene.Node;
import com.jme.scene.Spatial;
import com.jme.scene.state.WireframeState;
import com.jme.scene.state.ZBufferState;
import com.jme.system.DisplaySystem;

/**
 * Componentes del juego
 * <p>
 * Contiene toda la lógica relacionada con los estados de un componente y es la
 * clase base de la que hereda cualquier componente del juego.
 */
public abstract class GameComponent {

	/**
	 * Estados del juego.
	 */
    public final int ESTADO_INICIAL = 100;
    public final int ESTADO_ACTIVADO = 0;
    public final int ESTADO_FINALIZANDO = 109;
    public final int ESTADO_FIN = -1;

    /**
     * Control de la configuracion y el curso del juego.
     */
    public final int NINGUNA = 0;
    public final int JUGAR_1_JUGADOR = 1;
    public final int JUGAR_2_JUGADORES = 2;
    public final int SALIR_JUEGO = 10;
    

    private boolean active;
    private boolean visible;
    private int estado;
    private int condicion_fin;

    /**
     * Tiempo total de las animaciones.
     */
    protected float fin_animacion = 1.2f;
    
    /**
     * objeto encargado de reproducir, pausar y detener los sonidos del juego.
     */
    protected GestorMusica GM;
    
    /**
     * Tiempo transcurrido desde que se entro en el estado actual.
     */
    protected float inicio_estado;

    /**
     * Nodo raiz del componente.
     */
    protected Node root_node;

    /**
     * Display actualmente en uso.
     */
    protected DisplaySystem display;

    /**
     * Camara actualmente en uso.
     */
    protected Camera cam;

    /**
     * Interfaz hacia el dispositivo de entrada SunSpot.
     */
    protected Datos spot;

    // TODO: hay que meter el spot
    // protected SunSpotHostApplication input = new SunSpotHostAplication();

    public GameComponent (String name) {
        root_node = new Node(name);
        this.display = ComponentMannager.getDisplayInstance();
        this.cam = ComponentMannager.getCameraInstance();
        this.spot = ComponentMannager.getDatosInstance();
        this.GM = ComponentMannager.getGMInstance();

        /**
         * Crea un Z buffer para no pintar los pixeles que estan ocultos por otros
         */
        ZBufferState buf = display.getRenderer().createZBufferState();
        buf.setEnabled( true );
        buf.setFunction( ZBufferState.TestFunction.LessThanOrEqualTo );
        root_node.setRenderState( buf );

        //reiniciarComponente();
        this.setActive(true);
        this.setVisible(true);
        this.setEstate(ESTADO_INICIAL);
    }

    /**
     * Devuelve si el componente actual esta activado o no
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Pone el componente actual a activado o desactivado
     */
    private void setActive(boolean active) {
        this.active = active;
    }

    /**
     * Devuelve si el componente actual es visible
     */
    public boolean isVisible() {
        return visible;
    }

    /**
     * Pone al componente actual como visible o invisible
     */
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    /**
     * Pone al componente en el estado pasado por argumento
     * @param new_estate El nuevo estado al que cambia el componente
     */
    public void setEstate (int new_estate) {
        inicio_estado = 0.0f;
        estado = new_estate;
    }

    /**
     * Devuelve el numero del estado en el que se encuentra el componente
     * @return un numero correspondiente con el estado del componente
     */
    public int getEstate () {
        return estado;
    }

    /**
     * Cambia la duración de las animaciones de activado y desactivado.
     */
    public void duracionAnimacion (float tiempo) {
        this.fin_animacion = tiempo;
    }

    /**
     * Finaliza el uso del componente actual
     */
    public void terminar () {

        this.setCondicionFin(NINGUNA);
        this.setEstate(ESTADO_FINALIZANDO);
    }

    /**
     * Finaliza el uso del componente con un valor de retorno
     * @param condicion_fin indica la razón por la que se ha finalizado el componente
     */
    public void terminar (int condicion_fin) {
        this.setCondicionFin(condicion_fin);       
        this.setActive(false);
        this.setEstate(ESTADO_FIN);
    }

    /**
     * Devuelve la razón por la que el componente fué finalizado
     */
    public int getCondicionFin () {
        return this.condicion_fin;
    }

    public void setCondicionFin (int condicion_fin) {
        this.condicion_fin = condicion_fin;
    }

    /**
     * Devuelve el nodo raiz del componente
     */
    public Spatial getRootNode() {
        return root_node;
    }


    // Overraidable methods //
    
    /**
     * Accion que se ejecuta cuando se activa el componente
     * @param timeEnabling tiempo transcurrido desde que comenzo la animacion
     */
    protected void onEnabled (float timeEnabling) { }

    /**
     * Accioni que se ejecuta cuando se desactiva el componente
     * @param timeDisabling tiempo transcurrido desde que comenzo la animacion
     */
    protected void onDisabled (float timeDisabling) { }

    public void update(float tpf) {
        inicio_estado += tpf;
        switch (estado) {
            case ESTADO_INICIAL:
                this.onEnabled((inicio_estado > fin_animacion) ? fin_animacion : inicio_estado);
                if (inicio_estado >= fin_animacion)
                    this.setEstate(ESTADO_ACTIVADO);
                break;
            case ESTADO_FINALIZANDO:
                this.onDisabled((inicio_estado > fin_animacion) ? fin_animacion : inicio_estado);
                if (inicio_estado >= fin_animacion) {
                    this.setEstate(ESTADO_FIN);
                    this.setActive(false);
                }
                break;
        }
    }

    public void initGame() { }

    public void reiniciarComponente() {
        this.setActive(true);
        this.setVisible(true);
        this.setEstate(ESTADO_INICIAL);
    }

}
