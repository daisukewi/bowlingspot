package com.bowlingspot;



import java.util.ArrayList;
import com.bowlingspot.controller.Datos;
import com.bowlingspot.controller.ThreadSunSpotHost;
import com.bowlingspot.media.GestorMusica;
import com.jme.renderer.Camera;
import com.jme.system.DisplaySystem;
import com.jmex.physics.PhysicsSpace;

/**
 * Gestiona todos los componentes de nuestro juego. Es una clase est�tica y se encarga
 * de a�adir o borrar componentes as� como almacenar objetos necesarios como display,
 * camara, gestor de m�sica, espacio f�sico y el controlador del juego SunSpot
 * @author Daniel
 */
public final class ComponentMannager {

    /**
     * The added GameComponents
     */

    public static ArrayList<GameComponent> game_components = new ArrayList<GameComponent>();
    private static DisplaySystem display;
    private static Camera cam;
    private static Datos datos;
    private static boolean fin_juego = false;
    private static ThreadSunSpotHost tss;
    private static GestorMusica GM;
    private static PhysicsSpace physicsSpace;

    public static DisplaySystem getDisplayInstance() {
        return display;
    }
    public static void setDisplayInstance(DisplaySystem d) {
        display = d;
    }
    public static void setDatosInstance(Datos dts) {

        datos = dts;
    }                       
    public static Datos getDatosInstance()
    {
        return datos;
    }


    public static void addComponent (GameComponent gc) {
        game_components.add(gc);
    }
    public static void removeComponent (GameComponent gc) {
        game_components.remove(gc);
    }
    public static void setCammeraInstance(Camera camera) {
        cam = camera;
    }
    public static Camera getCameraInstance () {
        return cam;
    }
    public static void setFinJuego(boolean fin) {
        fin_juego = fin;
    }
    public static boolean isFinJuego() {
        return fin_juego;
    }
    public static void setTspInstance(ThreadSunSpotHost thss) {
        tss = thss;
    }
    public static ThreadSunSpotHost getTspInstance()
    {
       return tss;
    }
    
    public static void setGMInstance(GestorMusica GMusica)
    {
        GM=GMusica;
    }
    public static GestorMusica getGMInstance()
    {
        return GM;
    }

    public static PhysicsSpace getPhysicsSpace()
    {
        return physicsSpace;

    }

    public static void setPhysicsSpace(PhysicsSpace PS)
    {

        physicsSpace=PS;

    }

}
