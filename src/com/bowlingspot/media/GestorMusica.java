package com.bowlingspot.media;



/**
 *
 */
public class GestorMusica {

    protected ThreadMusica ATM[];

    public GestorMusica() {
        ATM = new ThreadMusica[10];
    }

    public int play(String song , float vol) {
        int i = 0;
        boolean colocado = false;
        while ((i < ATM.length) && !colocado) {
            if (ATM[i] == null || !ATM[i].Thm.isAlive()) {
                ATM[i] = new ThreadMusica(song,vol);
                colocado = true;
            }
            i++;
        }
        return i;
    }

    public void pause(int nt) {
        ATM[nt].pause();
    }

    public void stop(int nt) {
        ATM[nt].stop();
    }
    public void end(int nt)
    { ATM[nt] = null;}
    public boolean exist(int th)
    {
        return (ATM[th]!=null);
    }

    public boolean exist(String s)
    { boolean finded=false;

        for (int i=0;i<ATM.length;i++)
         { if (this.exist(i) && this.isalive(i))
           {
             if (ATM[i].nombrecancion.equals(s))
              { finded=true;
                i=100;}
           }

         }


     return finded;
    }

    public void Stop(String s)
    {
        for (int i=0;i<ATM.length;i++)
        {if (this.exist(i) && this.isalive(i))
           {
             if (ATM[i].nombrecancion.equals(s))
              { this.stop(i);
                this.end(i);
                i=100;}
           }
        }
    }

    public boolean isalive(int th)
    {  return (ATM[th].Thm.isAlive());}



    public int numsongs()
    {int aux=0;

        for (int i=0;i<ATM.length;i++)
        {
            if (ATM[i] != null && ATM[i].Thm.isAlive())
                 aux++;
        }

    return aux;
    }
}
