package com.bowlingspot.media;


import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.UnsupportedAudioFileException;


/**
 *
 */
public class ThreadMusica implements Runnable {

	private Musica musica;
	public Thread Thm;
	public String nombrecancion="";
      
      
	public ThreadMusica(String song , float Vol) {
		nombrecancion=song;
		alth(song,Vol);
	}

	@SuppressWarnings("static-access")
	public void pause() {
	    musica.pause();
	}
	
	public void notpause() {  
	    musica.continu();    
	}
	
	public void setrepeat(boolean rp) {
		musica.setrepeat(rp);
	}
	
	public void stop() {
		musica.stop();
	}

	public void alth(String song , float Vol) {
	    Thm = new Thread (this);
	    this.setMusica(song , Vol);
	    Thm.start();
	}

    public void setMusica (String str , float Vol) {
        musica = new Musica(str,Vol);
    }

    public void run() {
        try {
            musica.IniMusica();
        } catch (UnsupportedAudioFileException ex) {
            Logger.getLogger(ThreadMusica.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ThreadMusica.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
