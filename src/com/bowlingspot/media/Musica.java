package com.bowlingspot.media;



import java.io.File;
import java.io.IOException;
import java.net.URL;
import javax.sound.sampled.*;
import com.jme.util.resource.ResourceLocatorTool;


/**
*
*/
public class Musica {

    private static final int EXTERNAL_BUFFER_SIZE = 128000;
    private SourceDataLine line = null;
    private AudioInputStream audioInputStream = null;
    private AudioFormat audioFormat;
    private String strFilename= "";
    private float volumelevel=0;
    private boolean pause=false , stop=false, repeat=false;
    private Control[] CNT;
    private FloatControl volumen;


    public Musica(String fn , float VL)
    {
        this.strFilename=fn;
        this.volumelevel=VL;
    }
    public void IniMusica() throws UnsupportedAudioFileException, IOException {
        String music_path = "/res/music/";
        int nExternalBufferSize = EXTERNAL_BUFFER_SIZE;
        int nInternalBufferSize = AudioSystem.NOT_SPECIFIED;
        URL musicURL = ResourceLocatorTool.locateResource(
		        ResourceLocatorTool.TYPE_AUDIO, music_path + strFilename);
        File file = new File(musicURL.getFile());

        audioInputStream = AudioSystem.getAudioInputStream(file);
        audioFormat = audioInputStream.getFormat();
        line = getSourceDataLine(Mixer.Info.class.getName(), audioFormat, nInternalBufferSize);
        line.start();
        CNT = line.getControls();
        volumen=(FloatControl)line.getControl(FloatControl.Type.MASTER_GAIN);
        volumen.setValue(this.volumelevel);


       do {
        int nBytesRead = 0;
        byte[] abData = new byte[nExternalBufferSize];
        while (nBytesRead != -1 ) {
            while (!pause && nBytesRead !=-1)
            {
            try {
                if (repeat)
                {audioInputStream = AudioSystem.getAudioInputStream(file);
                audioFormat = audioInputStream.getFormat();}

                nBytesRead = audioInputStream.read(abData, 0, abData.length);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (nBytesRead >= 0) {
                    line.write(abData, 0, nBytesRead);

            if (stop) {nBytesRead=-1;}
            }
        }}} while (repeat && !stop);
        line.drain();
        line.close();




    }
 public void pause() {
     pause=true;
}
 public void setrepeat(boolean rp)
 {repeat=rp;}
 public boolean getrepeat()
 {return repeat;}

 public void continu() {
     pause=false;
 }
 public void stop(){
     stop=true;
 }
    private static SourceDataLine getSourceDataLine(String strMixerName,
            AudioFormat audioFormat,
            int nBufferSize) {
        SourceDataLine line = null;
        DataLine.Info info = new DataLine.Info(SourceDataLine.class,
                audioFormat, nBufferSize);
        try {
            if (strMixerName != null) {
                Mixer.Info mixerInfo = AudioCommon.getMixerInfo(strMixerName);
                if (mixerInfo == null) {
                }
                Mixer mixer = AudioSystem.getMixer(mixerInfo);
                line = (SourceDataLine) mixer.getLine(info);
            } else {
                line = (SourceDataLine) AudioSystem.getLine(info);
            }
            line.open(audioFormat, nBufferSize);
        } catch (LineUnavailableException e) {
        } catch (Exception e) {
        }
        return line;
    }


}
