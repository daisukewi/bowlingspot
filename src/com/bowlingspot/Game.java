package com.bowlingspot;



import java.util.logging.Level;
import java.util.logging.Logger;
import com.bowlingspot.controller.Datos;
import com.bowlingspot.controller.ThreadSunSpotHost;
import com.bowlingspot.media.GestorMusica;
import com.jme.app.FixedFramerateGame;
import com.jme.math.Vector3f;
import com.jme.renderer.Camera;
import com.jme.renderer.ColorRGBA;
import com.jme.renderer.Renderer;
import com.jme.system.DisplaySystem;
import com.jme.system.GameSettings;
import com.jme.system.JmeException;
import com.jme.system.PropertiesGameSettings;
import com.jmex.physics.PhysicsSpace;


/**
 * Clase base de nuestro juego. Carga los datos basicos necesarios para
 * que un juego funcione con propiedades f�sicas.
 */
public class Game extends FixedFramerateGame {

    /** Creacion de lo Necesario para el SunSpot**/
    protected   ThreadSunSpotHost thss = new ThreadSunSpotHost();
    private   Thread host = new Thread(thss);
    //*********FISICA******************///////////////
    private PhysicsSpace physicsSpace;
    protected boolean showPhysics;
    private float physicsSpeed = 1;
    //*********FISICA******************///////////////

    protected Datos datos;
    protected GestorMusica GM;

    private static final Logger logger = Logger.getLogger(BowlingSpot.class
            .getName());

    /**
     * Alpha bits to use for the renderer. Any changes must be made prior to
     * call of start().
     */
    protected int alphaBits = 0;

    /**
     * Depth bits to use for the renderer. Any changes must be made prior to
     * call of start().
     */
    protected int depthBits = 8;

    /**
     * Stencil bits to use for the renderer. Any changes must be made prior to
     * call of start().
     */
    protected int stencilBits = 0;

    /**
     * Number of samples to use for the multisample buffer. Any changes must be
     * made prior to call of start().
     */
    protected int samples = 0;

    /**
     * The camera that we see through.
     */
    protected Camera cam;

    /**
     * Simply an easy way to get at timer.getTimePerFrame(). Also saves math cycles since
     * you don't call getTimePerFrame more than once per frame.
     */
    protected float tpf;

    

    public Game () {
        super();
    }

    @Override
    protected void initSystem() throws JmeException {
        try {
            //***********************FISICA********//////
            setPhysicsSpace( PhysicsSpace.create() );
            //***********************FISICA***********/////
            /**
             * Get a DisplaySystem acording to the renderer selected in the
             * startup box.
             */
            display = DisplaySystem.getDisplaySystem(settings.getRenderer() );

            display.setMinDepthBits( depthBits );
            display.setMinStencilBits( stencilBits );
            display.setMinAlphaBits( alphaBits );
            display.setMinSamples( samples );

            /** Create a window with the startup box's information. */
            display.createWindow(settings.getWidth(), settings.getHeight(),
                    settings.getDepth(), settings.getFrequency(),
                    settings.isFullscreen() );
    
            /**
             * Create a camera specific to the DisplaySystem that works with the
             * display's width and height
             */
            cam = display.getRenderer().createCamera( display.getWidth(),
                    display.getHeight() );

        } catch ( JmeException e ) {
            /**
             * If the displaysystem can't be initialized correctly, exit
             * instantly.
             */
            logger.log(Level.SEVERE, "Could not create displaySystem", e);
            System.exit( 1 );
        }

        /** Set an orange background. */
        display.getRenderer().setBackgroundColor( ColorRGBA.black.clone() );

        /** Set up how our camera sees. */
        cameraPerspective();
        Vector3f loc = new Vector3f( 0.0f, 0.0f, 25.0f );
        Vector3f left = new Vector3f( -1.0f, 0.0f, 0.0f );
        Vector3f up = new Vector3f( 0.0f, 1.0f, 0.0f );
        Vector3f dir = new Vector3f( 0.0f, 0f, -1.0f );
        /** Move our camera to a correct place and orientation. */
        cam.setFrame( loc, left, up, dir );
        /** Signal that we've changed our camera's location/frustum. */
        cam.update();
        /** Assign the camera to this renderer. */
        display.getRenderer().setCamera( cam );

        display.setTitle( "BowlingSpot" );
        
        GM = new GestorMusica();
        datos = new Datos();
        
        ComponentMannager.setPhysicsSpace(physicsSpace);
        ComponentMannager.setDisplayInstance(display);
        ComponentMannager.setCammeraInstance(cam);
        ComponentMannager.setDatosInstance(datos);
        ComponentMannager.setTspInstance(thss);
        ComponentMannager.setGMInstance(GM);

        ComponentMannager.setFinJuego(false);
        ComponentMannager.addComponent(new BowlingSpot());
    }

    protected void cameraPerspective() {
        cam.setFrustumPerspective( 45.0f, (float) display.getWidth()
                / (float) display.getHeight(), 1, 1000 );
        cam.setParallelProjection( false );
        cam.update();
    }

    protected void cameraParallel() {
        cam.setParallelProjection( true );
        float aspect = (float) display.getWidth() / display.getHeight();
        cam.setFrustum( -100, 1000, -50 * aspect, 50 * aspect, -50, 50 );
        cam.update();
    }

    @Override
    protected GameSettings getNewSettings() {
        return new MyGameSettings();
    }

    static class MyGameSettings extends PropertiesGameSettings {
        static {
            defaultFullscreen = Boolean.TRUE;
        }

        MyGameSettings() {
            super("properties.cfg");
            load();
        }
    }

    @Override
    protected void initGame() {
        host.start();
        for (int i = 0; i < ComponentMannager.game_components.size(); i++) {
            ComponentMannager.game_components.get(i).initGame();
        }
    }

    @Override
    protected void update(float interpolation) {
        if (thss.getconected()) {
            ComponentMannager.setDatosInstance(datos=thss.getdatos());
        }

        tpf = (float)1/60;
        if (ComponentMannager.isFinJuego()) {
            this.finish();
        } else {
        	// Update Physics
        	if ( tpf > 0.2 || Float.isNaN( tpf ) ) {
                Logger.getLogger( PhysicsSpace.LOGGER_NAME ).warning( "Maximum physics update interval is 0.2 seconds - capped." );
                tpf = 0.2f;
            }
            getPhysicsSpace().update( tpf * physicsSpeed );
            
            // Update Game
            int componentes = ComponentMannager.game_components.size();
            for (int i = 0; i < componentes ; i++) {
                GameComponent componente = ComponentMannager.game_components.get(i);
                if (componente.isActive()) {
                    componente.update(tpf);
                    componente.getRootNode().updateGeometricState(tpf, true);
                }
                componentes = ComponentMannager.game_components.size();
            }
        }
    }

    @Override
    protected void render(float interpolation) {
        Renderer r = display.getRenderer();
        int cantidad = ComponentMannager.game_components.size();
        /** Clears the previously rendered information. */
        r.clearBuffers();

        for (int i = 0; i < cantidad; i++) {
            GameComponent componente = (GameComponent)ComponentMannager.game_components.get(i);
            if (componente.isVisible()) {
                componente.getRootNode().updateRenderState();
                r.draw(componente.getRootNode());
            }
        }
    }

    @Override
    protected void reinit() {
    }

    @Override
    protected void cleanup() {
        if (display != null && display.getRenderer() != null)
            display.getRenderer().cleanup();
    }
    
 //***********FISICA***********************//
     private void setPhysicsSpace(PhysicsSpace physicsSpace) {
		if ( physicsSpace != this.physicsSpace ) {
			if ( this.physicsSpace != null )
	       		this.physicsSpace.delete();
			this.physicsSpace = physicsSpace;
		}
	}

    private PhysicsSpace getPhysicsSpace() {
        return physicsSpace;
    }
//***********FISICA***********************//
}
